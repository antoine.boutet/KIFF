package kiff.systems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;

import gnu.trove.map.hash.TIntDoubleHashMap;
import kiff.util.ScoreCount;
import kiff.util.Measures;
import kiff.util.MyPeer;
import kiff.util.MyPeerFKNN;
import kiff.util.ProfileLoader;
import kiff.util.Tools;

public class KIFF {
	
	private static Random random = new Random();
	
	public static Map<Integer, ScoreCount<Integer>> userProfiles;
	public static Map<Integer, Set<Integer>> itemProfiles;
	
	public static ConcurrentHashMap<Integer, MyPeer> mapping;
	public static Map<Integer, TIntDoubleHashMap> best;
	
	private static String resultsPath;
	private static int sizeknn;	
	private static int cycleTerminaison;
	private static Boolean log; 
	private static int sizeBlocFilter;
	private static double terminationThreshold;
	private static Boolean bestMatch;
	private static String bestMatchPath;


	public KIFF(int k, 
			int dataset, 
			double t, 
			boolean b,
			String r,
			int c,
			boolean l,
			String bs) {
		
		
		terminationThreshold = t;
		bestMatch = b;
		resultsPath= r;
		cycleTerminaison = c;
		log = l;
		sizeBlocFilter = k;
		bestMatchPath = bs;
			
		userProfiles = new HashMap<>();
		itemProfiles = new HashMap<>();
		
		sizeknn = k;
		
		long tprofile1 = System.nanoTime();

		// build user and item profile
		loadDataset(dataset);
		
		long tprofile2 = System.nanoTime();
		
//		sim = new Simulator<MyPeer>();		
		mapping = new ConcurrentHashMap<Integer, MyPeer>();		
		
		for(Integer uid : userProfiles.keySet()) {
			MyPeerFKNN p = new MyPeerFKNN(uid, random, userProfiles.get(uid), sizeknn);
//			sim.addPeer(p);
			mapping.put(uid, p);		
		}	
		
		long tpreproc1 = System.nanoTime();
		
		List<Integer> users = new ArrayList<Integer>(userProfiles.keySet());
		
		users.parallelStream().forEach(u -> preprocessing(u));
		
		long tpreproc2 = System.nanoTime();
		
		if(log==true) {	
			
			if(!bestMatch) {				
				best = Tools.loadBest(bestMatchPath+".object");		
				Measures.recallMeasure(mapping, resultsPath, sizeknn, best);
			}
			
			Measures.candidateSetSizeMeasure(mapping, resultsPath);
		}
			
		int iteration = 0;
		
		while(true) {
			
			//log Tools.log(logFilename, "\n"+iteration + " ");
			//log System.out.println("iteration " + iteration);
			
			users.parallelStream().forEach(u -> candidateSelection(u));
			
			users.parallelStream().forEach(u -> clustering(u));

			//candidateSetSizeMeasure();
			

			if(log==true) {	
				
				Tools.log(resultsPath, "\n"+iteration + " ");
				System.out.println(iteration);
				
				if(!bestMatch) {
					Measures.recallMeasure(mapping, resultsPath, sizeknn, best);
				}
							
				Measures.nbSimilarityEvaluationMeasure(mapping, resultsPath);
				Measures.candidateSetSizeMeasure(mapping, resultsPath);
				Measures.nbUpdateMeasure(mapping, resultsPath, terminationThreshold);			
			}
			
			if(bestMatch) {				
				if(terminaisonItemBasedCandidateSetSize()) {				
					break;
				}
			}
			else {				
				if(terminaison(terminationThreshold)) {				
					break;
				}	
			}
			
			if(iteration==cycleTerminaison) {
				break;
			}
			
			iteration++;
		}
		
		long tprocessing = System.nanoTime();
				
		
		System.out.println("iteration " + iteration);
		
		long readProfile =tprofile2-tprofile1;
		long tpreproc =tpreproc2-tpreproc1;
		long computation = (tprocessing-tpreproc2);
		long global = tprocessing-tprofile1;

		System.out.println("time profile construction " + TimeUnit.MILLISECONDS.convert(readProfile, TimeUnit.NANOSECONDS));
		System.out.println("time preproc " + TimeUnit.MILLISECONDS.convert(tpreproc, TimeUnit.NANOSECONDS));
		System.out.println("time computation " + TimeUnit.MILLISECONDS.convert(computation, TimeUnit.NANOSECONDS));
		System.out.println("time global " + TimeUnit.MILLISECONDS.convert(global, TimeUnit.NANOSECONDS));
		
		Tools.log(resultsPath, "\ntime profile construction " + TimeUnit.MILLISECONDS.convert(readProfile, TimeUnit.NANOSECONDS));
		Tools.log(resultsPath, "\ntime preproc " + TimeUnit.MILLISECONDS.convert(tpreproc, TimeUnit.NANOSECONDS));
		Tools.log(resultsPath, "\ntime computation " + TimeUnit.MILLISECONDS.convert(computation, TimeUnit.NANOSECONDS));
		Tools.log(resultsPath, "\ntime global " + TimeUnit.MILLISECONDS.convert(global, TimeUnit.NANOSECONDS)+"\n");
		
		
		
		if(!bestMatch) {
			
			if(!log) {
				best = Tools.loadBest(bestMatchPath+".object");
			}		
				
			Measures.recallMeasure(mapping, resultsPath, sizeknn, best);
		}
						
		Measures.nbSimilarityEvaluationMeasure(mapping, resultsPath);	
		Measures.nbUpdateMeasure(mapping, resultsPath, terminationThreshold);
	}
	



	private static void preprocessing(Integer uid) {
		
		Multiset<Integer> mset = HashMultiset.create();
		
		// for each rated items
		for(Integer rid : userProfiles.get(uid).getItems()) {
				
				// for each users who rated this item
				for(Integer uuid: itemProfiles.get(rid)) {
					
					if(uuid.compareTo(uid)==0) {
						continue;
					}
					
					if(uuid>=uid) {
						continue;
					}
											
					mset.add(uuid);
				}				
		}
		
		MyPeerFKNN p = (MyPeerFKNN)mapping.get(uid);
		p.setItemBasedCandidates(new ArrayList<Integer>(Multisets.copyHighestCountFirst(mset).elementSet()));
	}
	
	private static void candidateSelection(Integer uid) {
		
		MyPeerFKNN me = (MyPeerFKNN)mapping.get(uid);
		
		me.cleanUpdate();		
		me.getCandidates().clear();
		
		while(me.getCandidates().size()<(2*sizeknn) && me.getItemBasedCandidates().size()>0) {
			
			Integer id = me.getItemBasedCandidates().get(0);
			me.getItemBasedCandidates().remove(0);
						
			me.getCandidates().add(id);
		}
		
	}
	
	private static void clustering(Integer uid) {
		
		MyPeerFKNN me = (MyPeerFKNN)mapping.get(uid);

		double note;

		// random
		for(Integer u1 : me.getCandidates()) {
			
			// compute the distance
			note = mapping.get(uid).getMyProfile().getCos(mapping.get(u1).getMyProfile());
			me.incNbSimilarityEvaluation();
						
			
			boolean inserted =  mapping.get(uid).updateNeighborhood(u1, note);
//			boolean inserted =  mapping.get(this.peer.getPeerId()).getHeapFriends().offer(new HeapNode(u1, note, mapping.get(u1), true));
			if(inserted) {
				mapping.get(uid).incUpdate();
			}
				
			inserted =  mapping.get(u1).updateNeighborhood(uid, note);
//			inserted = mapping.get(u1).getHeapFriends().offer(new HeapNode(this.peer.getPeerId(), note, this.peer, true));
			if(inserted) {						
				mapping.get(u1).incUpdate();						
			}						
		}		
		
		me.incIteration();
		
	}
	
	

	private static boolean terminaison(double t) {
		
		double nb=0;
		
		for (Integer uid : mapping.keySet()) {
			
			nb += mapping.get(uid).getUpdate();		
			
		}
				
		nb /= mapping.size();
		
		if(nb <= t) {				
			return true;
		}	
		
		return false;
	}
	
		

	private static void loadDataset(int dataset) {
		

		if(dataset==-1) {
			
			resultsPath += "ml2-1-ratings-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-1-ratings-k"+sizeknn;
			
			userProfiles = Tools.loadProfile("ml2-ratings1000209.object");

			itemProfiles = new HashMap<>();
			
			for(Integer uid : userProfiles.keySet()) {
				for(Integer rid : userProfiles.get(uid).getItems()) {
						if(!itemProfiles.containsKey(rid)) {
							itemProfiles.put(rid, new HashSet<>());
						}
						itemProfiles.get(rid).add(uid);
				}
			}
			 
		}
		else if(dataset==-2) {
			
			resultsPath += "ml2-2-ratings-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-2-ratings-k"+sizeknn;
			
			userProfiles = Tools.loadProfile("ml2-ratings500009.object");

			itemProfiles = new HashMap<>();
			
			for(Integer uid : userProfiles.keySet()) {
				for(Integer rid : userProfiles.get(uid).getItems()) {
						if(!itemProfiles.containsKey(rid)) {
							itemProfiles.put(rid, new HashSet<>());
						}
						itemProfiles.get(rid).add(uid);
				}
			}
			 
		}
		else if(dataset==-3) {
			
			resultsPath += "ml2-3-ratings-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-3-ratings-k"+sizeknn;
			
			userProfiles = Tools.loadProfile("ml2-ratings255188.object");

			itemProfiles = new HashMap<>();
			
			for(Integer uid : userProfiles.keySet()) {
				for(Integer rid : userProfiles.get(uid).getItems()) {
						if(!itemProfiles.containsKey(rid)) {
							itemProfiles.put(rid, new HashSet<>());
						}
						itemProfiles.get(rid).add(uid);
				}
			}
			 
		}
		else if(dataset==-4) {
			
			resultsPath += "ml2-4-ratings-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-4-ratings-k"+sizeknn;
			
			userProfiles = Tools.loadProfile("ml2-ratings131668.object");

			itemProfiles = new HashMap<>();
			
			for(Integer uid : userProfiles.keySet()) {
				for(Integer rid : userProfiles.get(uid).getItems()) {
						if(!itemProfiles.containsKey(rid)) {
							itemProfiles.put(rid, new HashSet<>());
						}
						itemProfiles.get(rid).add(uid);
				}
			}
			 
		}
		else if(dataset==-5) {
			
			resultsPath += "ml2-5-ratings-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-5-ratings-k"+sizeknn;
			
			userProfiles = Tools.loadProfile("ml2-ratings68415.object");

			itemProfiles = new HashMap<>();
			
			for(Integer uid : userProfiles.keySet()) {
				for(Integer rid : userProfiles.get(uid).getItems()) {
						if(!itemProfiles.containsKey(rid)) {
							itemProfiles.put(rid, new HashSet<>());
						}
						itemProfiles.get(rid).add(uid);
				}
			}
			 
		}
		else if(dataset==0) {
			
			resultsPath += "ml1-k"+sizeknn;
			bestMatchPath += "bestKNN-ml1-k"+sizeknn;
			ProfileLoader.UsersItemsStarValueProfileLoader("dataset/ml-100k/u.data", userProfiles, itemProfiles);
		}
		else if(dataset==1) {
			
			resultsPath += "ml2-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-k"+sizeknn;
			ProfileLoader.UsersItemsStarValueProfileLoader("dataset/ml-1m/ratings.dat", userProfiles, itemProfiles);			
		}
		else if(dataset==2) {
			
			resultsPath += "arxiv1-k"+sizeknn;	
			bestMatchPath += "bestKNN-arxiv1-k"+sizeknn;
			ProfileLoader.UsersItemsProfileLoader("dataset/Arxiv/CA-GrQc.txt", userProfiles, itemProfiles);
		}
		else if(dataset==3) {
			
			resultsPath += "arxiv2-k"+sizeknn+"-g"+sizeBlocFilter+"-t"+terminationThreshold;	
			bestMatchPath += "bestKNN-arxiv2-k"+sizeknn;
			ProfileLoader.UsersItemsProfileLoader("dataset/Arxiv/CA-AstroPh.txt", userProfiles, itemProfiles);
		}
		else if(dataset==4) {
			
			resultsPath += "wikipedia-k"+sizeknn+"-g"+sizeBlocFilter+"-t"+terminationThreshold;	
			bestMatchPath += "bestKNN-wikipedia-k"+sizeknn;
			ProfileLoader.UsersItemsProfileLoader("dataset/wikipedia/Wiki-Vote.txt", userProfiles, itemProfiles);
		}
		else if(dataset==5) {
			
			resultsPath += "gowalla-k"+sizeknn+"-g"+sizeBlocFilter+"-t"+terminationThreshold;
			bestMatchPath += "bestKNN-gowalla-k"+sizeknn;
			ProfileLoader.UsersItemsValueProfileLoader("dataset/loc-gowalla/Gowalla_totalCheckins.txt-value", userProfiles, itemProfiles);
		
		}
		else if(dataset==6) {
			
			resultsPath += "dblp-k"+sizeknn+"-g"+sizeBlocFilter+"-t"+terminationThreshold;
			bestMatchPath += "bestKNN-dblp-k"+sizeknn;
			ProfileLoader.UsersItemsValueProfileLoader("dataset/dblp/dblp.xml.value.clean.filter5.txt", userProfiles, itemProfiles);
			
		}
		
	}
	
	private static boolean terminaisonItemBasedCandidateSetSize() {
		
		for (Integer uid : mapping.keySet()) {
			
			MyPeerFKNN p1 = (MyPeerFKNN)mapping.get(uid);
			
			if(p1.getItemBasedCandidates().size()>0) {
				return false;
			}	
		}
		
		return true;
	}
	
	
	public static void main(String[] args) {
				  
//		if(args.length<8) {
//			System.out.println("USAGE: {dataset 3:arxiv 4:wikipedia 5:gowalla 6:dbpedia} "
//					+ "k cycleTermination thresholdTermination best(boolean) log(boolean) resultsPath bestPath");
//			System.exit(0);
//		}
//		
//		int dataset = Integer.parseInt(args[0]);			// dataset
//		int k = Integer.parseInt(args[1]);					// size knn
//		int c = Integer.parseInt(args[2]);					// cycleTerminaison
//		double t = Double.parseDouble(args[3]);				//  terminationThreshold
//		Boolean b = Boolean.parseBoolean(args[4]); 			// 	bestMatch
//		Boolean l = Boolean.parseBoolean(args[5]); 			// log
//		String r = args[6]; 								// resultsPath
//		String bestKNNPath = args[7];						// string best knn (for recall measurement)
		
		
		int dataset = 3;					// dataset
		int k = 20;							// size knn
		int c = 100; 						// cycleTerminaison		
		double t = 0.001; 					//  terminationThreshold
		Boolean b = false; 					// 	bestMatch
		Boolean l = false; 					// log
		String r = "results/kiff-"; 				// resultsPath
		String bestKNNPath = "bestKNN/";	// string best knn (for recall measurement)
		
		KIFF kiff = new KIFF(k, dataset, t, b, r, c, l, bestKNNPath);
		
		
		
		if(b) {			
			// get the knn graph with the distance between users
			Map<Integer, TIntDoubleHashMap> knn = Tools.getKNNValueGraph(userProfiles, mapping);
					
			Tools.saveGraphValue(bestMatchPath, knn);
			
			Tools.saveObject(bestMatchPath, knn);
		}
		
	}
}




