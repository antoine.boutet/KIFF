package kiff.systems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import gnu.trove.iterator.TIntIntIterator;
import gnu.trove.map.TIntDoubleMap;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.hash.TIntDoubleHashMap;
import gnu.trove.map.hash.TIntIntHashMap;
import kiff.util.ScoreCount;
import kiff.util.Measures;
import kiff.util.MyPeer;
import kiff.util.MyPeerFKNN;
import kiff.util.ProfileLoader;
import kiff.util.Tools;

public class CosineBaseline {
	
	private static Random random = new Random();

	private static Map<Integer, Set<Integer>> itemProfiles;
	private static Map<Integer, ScoreCount<Integer>> userProfiles;
	private static TIntDoubleMap userNormalization = new TIntDoubleHashMap();
	
	public static ConcurrentHashMap<Integer, MyPeer> mapping;
	public static Map<Integer, TIntDoubleHashMap> best;
	
	private static String resultsPath;
	private static int sizeknn;	
	private static String bestMatchPath;


	public CosineBaseline(int k, 
			int dataset, 
			String r,
			String bs) {
		
		
		resultsPath= r;
		bestMatchPath = bs;
			
		userProfiles = new HashMap<>();
		itemProfiles = new HashMap<>();
		
		sizeknn = k;
		
		long tprofile1 = System.nanoTime();

		// build user and item profile
		loadDataset(dataset);
		
		long tprofile2 = System.nanoTime();
			
		mapping = new ConcurrentHashMap<Integer, MyPeer>();		
		
		for(Integer uid : userProfiles.keySet()) {
			MyPeerFKNN p = new MyPeerFKNN(uid, random, userProfiles.get(uid), sizeknn);
			mapping.put(uid, p);		
		}	
		
		long tpreproc1 = System.nanoTime();
		
		List<Integer> users = new ArrayList<Integer>(userProfiles.keySet());
		
		for (Integer u : users) {
			userNormalization.put(u, Math.sqrt(userProfiles.get(u).size()));
		}
		
//		users.parallelStream().forEach(u -> preprocessing(u));
		
		long tpreproc2 = System.nanoTime();
		
		ConcurrentHashMap<Integer, int[]> topk = new ConcurrentHashMap<>();
		
		users.parallelStream().forEach(u -> topk.put(u, computeTopK(u)));
		
				
		long tprocessing = System.nanoTime();
				
		
		long readProfile =tprofile2-tprofile1;
		long tpreproc =tpreproc2-tpreproc1;
		long computation = (tprocessing-tpreproc2);
		long global = tprocessing-tprofile1;

		System.out.println("time profile construction " + TimeUnit.MILLISECONDS.convert(readProfile, TimeUnit.NANOSECONDS));
		System.out.println("time preproc " + TimeUnit.MILLISECONDS.convert(tpreproc, TimeUnit.NANOSECONDS));
		System.out.println("time computation " + TimeUnit.MILLISECONDS.convert(computation, TimeUnit.NANOSECONDS));
		System.out.println("time global " + TimeUnit.MILLISECONDS.convert(global, TimeUnit.NANOSECONDS));
		
		Tools.log(resultsPath, "\ntime profile construction " + TimeUnit.MILLISECONDS.convert(readProfile, TimeUnit.NANOSECONDS));
		Tools.log(resultsPath, "\ntime preproc " + TimeUnit.MILLISECONDS.convert(tpreproc, TimeUnit.NANOSECONDS));
		Tools.log(resultsPath, "\ntime computation " + TimeUnit.MILLISECONDS.convert(computation, TimeUnit.NANOSECONDS));
		Tools.log(resultsPath, "\ntime global " + TimeUnit.MILLISECONDS.convert(global, TimeUnit.NANOSECONDS)+"\n");
		
		best = Tools.loadBest(bestMatchPath+".object");
		Measures.recallMeasure(mapping, resultsPath, sizeknn, best);
						
		Measures.nbSimilarityEvaluationMeasure(mapping, resultsPath);	
	}
	
	
	public static int[] computeTopK(Integer user) {
		int currentlyIn = 0;
		TIntIntMap overlapCount = new TIntIntHashMap();
		for (Integer item : userProfiles.get(user).getItems()) {
			for (Integer u : itemProfiles.get(item)) {
				if (u != user) {
					overlapCount.adjustOrPutValue(u, 1, 1);
				}
			}
		}
		if (overlapCount.size() <= sizeknn) {
			return overlapCount.keys();
		} else {
			final int[] topk = new int[sizeknn];
			final double[] scores = new double[sizeknn];
			TIntIntIterator iter = overlapCount.iterator();
			while (iter.hasNext()) {
				iter.advance();
				final double score = iter.value() / userNormalization.get(iter.key());
				currentlyIn = updateTop(topk, scores, currentlyIn, iter.key(), score);
			}
			return topk;
		}
	}

	public static int updateTop(final int[] topk, final double[] scores, final int currentlyIn, final int user,
			final double score) {
		if (currentlyIn < sizeknn) {
			int insertPos;
			for (insertPos = currentlyIn - 1; insertPos >= 0 && score > scores[insertPos]; insertPos--) {
			}
			insertPos++;
			for (int shiftPos = currentlyIn; shiftPos > insertPos; shiftPos--) {
				scores[shiftPos] = scores[shiftPos - 1];
				topk[shiftPos] = topk[shiftPos - 1];
			}
			scores[insertPos] = score;
			topk[insertPos] = user;
			return currentlyIn + 1;
		} else if (score > scores[sizeknn - 1]) {
			int insertPos;
			for (insertPos = sizeknn - 2; insertPos >= 0 && score > scores[insertPos]; insertPos--) {
			}
			insertPos++;
			for (int shiftPos = sizeknn - 1; shiftPos > insertPos; shiftPos--) {
				scores[shiftPos] = scores[shiftPos - 1];
				topk[shiftPos] = topk[shiftPos - 1];
			}
			scores[insertPos] = score;
			topk[insertPos] = user;
			return currentlyIn;
		} else {
			return currentlyIn;
		}
	}


	private static void loadDataset(int dataset) {
		

		if(dataset==-1) {
			
			resultsPath += "ml2-1-ratings-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-1-ratings-k"+sizeknn;
			
			userProfiles = Tools.loadProfile("ml2-ratings1000209.object");

			itemProfiles = new HashMap<>();
			
			for(Integer uid : userProfiles.keySet()) {
				for(Integer rid : userProfiles.get(uid).getItems()) {
						if(!itemProfiles.containsKey(rid)) {
							Set<Integer> set = new HashSet<>();
							itemProfiles.put(rid, set);
						}
						itemProfiles.get(rid).add(uid);
				}
			}
			 
		}
		else if(dataset==-2) {
			
			resultsPath += "ml2-2-ratings-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-2-ratings-k"+sizeknn;
			
			userProfiles = Tools.loadProfile("ml2-ratings500009.object");

			itemProfiles  = new HashMap<>();
			
			for(Integer uid : userProfiles.keySet()) {
				for(Integer rid : userProfiles.get(uid).getItems()) {
						if(!itemProfiles.containsKey(rid)) {
							Set<Integer> set = new HashSet<>();
							itemProfiles.put(rid, set);
						}
						itemProfiles.get(rid).add(uid);
				}
			}
			 
		}
		else if(dataset==-3) {
			
			resultsPath += "ml2-3-ratings-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-3-ratings-k"+sizeknn;
			
			userProfiles = Tools.loadProfile("ml2-ratings255188.object");

			itemProfiles  = new HashMap<>();
			
			for(Integer uid : userProfiles.keySet()) {
				for(Integer rid : userProfiles.get(uid).getItems()) {
						if(!itemProfiles.containsKey(rid)) {
							Set<Integer> set = new HashSet<>();
							itemProfiles.put(rid, set);
						}
						itemProfiles.get(rid).add(uid);
				}
			}
			 
		}
		else if(dataset==-4) {
			
			resultsPath += "ml2-4-ratings-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-4-ratings-k"+sizeknn;
			
			userProfiles = Tools.loadProfile("ml2-ratings131668.object");

			itemProfiles = new HashMap<>();
			
			for(Integer uid : userProfiles.keySet()) {
				for(Integer rid : userProfiles.get(uid).getItems()) {
						if(!itemProfiles.containsKey(rid)) {
							Set<Integer> set = new HashSet<>();
							itemProfiles.put(rid, set);
						}
						itemProfiles.get(rid).add(uid);
				}
			}
			 
		}
		else if(dataset==-5) {
			
			resultsPath += "ml2-5-ratings-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-5-ratings-k"+sizeknn;
			
			userProfiles = Tools.loadProfile("ml2-ratings68415.object");

			itemProfiles = new HashMap<>();
			
			for(Integer uid : userProfiles.keySet()) {
				for(Integer rid : userProfiles.get(uid).getItems()) {
						if(!itemProfiles.containsKey(rid)) {
							Set<Integer> set = new HashSet<>();
							itemProfiles.put(rid, set);
						}
						itemProfiles.get(rid).add(uid);
				}
			}
			 
		}
		else if(dataset==0) {
			
			resultsPath += "ml1-k"+sizeknn;
			bestMatchPath += "bestKNN-ml1-k"+sizeknn;
			ProfileLoader.UsersItemsStarValueProfileLoader("dataset/ml-100k/u.data", userProfiles, itemProfiles);
		}
		else if(dataset==1) {
			
			resultsPath += "ml2-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-k"+sizeknn;
			ProfileLoader.UsersItemsStarValueProfileLoader("dataset/ml-1m/ratings.dat", userProfiles, itemProfiles);			
		}
		else if(dataset==2) {
			
			resultsPath += "arxiv1-k"+sizeknn;	
			bestMatchPath += "bestKNN-arxiv1-k"+sizeknn;
			ProfileLoader.UsersItemsProfileLoader("dataset/Arxiv/CA-GrQc.txt", userProfiles, itemProfiles);
		}
		else if(dataset==3) {
			
			resultsPath += "arxiv2-k"+sizeknn;	
			bestMatchPath += "bestKNN-arxiv2-k"+sizeknn;
			ProfileLoader.UsersItemsProfileLoader("dataset/Arxiv/CA-AstroPh.txt", userProfiles, itemProfiles);
		}
		else if(dataset==4) {
			
			resultsPath += "wikipedia-k"+sizeknn;	
			bestMatchPath += "bestKNN-wikipedia-k"+sizeknn;
			ProfileLoader.UsersItemsProfileLoader("dataset/wikipedia/Wiki-Vote.txt", userProfiles, itemProfiles);
		}
		else if(dataset==5) {
			
			resultsPath += "gowalla-k"+sizeknn;
			bestMatchPath += "bestKNN-gowalla-k"+sizeknn;
			ProfileLoader.UsersItemsValueProfileLoader("dataset/loc-gowalla/Gowalla_totalCheckins.txt-value", userProfiles, itemProfiles);
		
		}
		else if(dataset==6) {
			
			resultsPath += "dblp-k"+sizeknn;
			bestMatchPath += "bestKNN-dblp-k"+sizeknn;
			ProfileLoader.UsersItemsValueProfileLoader("dataset/dblp/dblp.xml.value.clean.filter5.txt", userProfiles, itemProfiles);
			
		}
		
	}
	
	
	
	public static void main(String[] args) {
				  
//		if(args.length<4) {
//			System.out.println("USAGE: {dataset 3:arxiv 4:wikipedia 5:gowalla 6:dbpedia} "
//					+ "k log(boolean) resultsPath bestPath");
//			System.exit(0);
//		}
//		
//		int dataset = Integer.parseInt(args[0]);			// dataset
//		int k = Integer.parseInt(args[1]);					// size knn
//		String r = args[2]; 								// resultsPath
//		String bestKNNPath = args[3];						// string best knn (for recall measurement)
		
		
		int dataset = 4;					// dataset
		int k = 20;							// size knn
		String r = "results/cosine-"; 				// resultsPath
		String bestKNNPath = "bestKNN/";	// string best knn (for recall measurement)
		
		CosineBaseline kiff = new CosineBaseline(k, dataset, r, bestKNNPath);
		
	}
}




