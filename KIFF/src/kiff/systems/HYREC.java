package kiff.systems;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import gnu.trove.map.hash.TIntDoubleHashMap;
import kiff.util.MyPeerNNDesc;
import kiff.util.ProfileLoader;
import kiff.util.ScoreCount;
import kiff.util.Measures;
import kiff.util.MyPeer;
import kiff.util.MyPeerFKNN;
import kiff.util.MyPeerHyRec;
import kiff.util.Tools;

public class HYREC {
	
	private static Random random = new Random();
	
	public static Hashtable<Integer, ScoreCount<Integer>> userProfiles;
	public static List<Integer> users;
	
	public static ConcurrentHashMap<Integer, MyPeer> mapping;
	public static Hashtable<Integer, TIntDoubleHashMap> best;
	
	private static String resultsPath;
	private static int sizeknn;	
	private static int cycleTerminaison;
	private static Boolean log; 
	private static double terminationThreshold;
	private static String bestMatchPath;
	private static int sizeRandom;

	
	public HYREC(int k, 
			int dataset, 
			double t, 
			String r,
			int c,
			boolean l,
			String bs,
			int sr) {
		
		
		terminationThreshold = t;
		resultsPath= r;
		cycleTerminaison = c;
		log = l;
		bestMatchPath = bs;
		sizeRandom = sr;
		
		userProfiles = new Hashtable<Integer, ScoreCount<Integer>>();
		
		sizeknn = k;
		
		long tprofile1 = System.nanoTime();

		// build user and item profile
		loadDataset(dataset);
		
		users = new ArrayList<Integer>(userProfiles.keySet());
		
		long tprofile2 = System.nanoTime();
		
		mapping = new ConcurrentHashMap<Integer, MyPeer>();		
		
		for(Integer uid : userProfiles.keySet()) {
			MyPeer p = new MyPeerHyRec(uid, random, userProfiles.get(uid), sizeknn);
			mapping.put(uid, p);		
		}	
		
		long tpreproc1 = System.nanoTime();
		
		users.parallelStream().forEach(u -> initKNNRandomly(u));
		
		long tpreproc2 = System.nanoTime();
		
		if(log==true) {	
			
			best = Tools.loadBest(bestMatchPath+".object");		
			Measures.recallMeasure(mapping, resultsPath, sizeknn, best);
		}
			
		int iteration = 0;
		
		long tcs = 0;
		
		while(true) {

			long tcs1 = System.nanoTime();
			
			// update listes
			users.parallelStream().forEach(u -> updateNewOldList(u));
			reverseNewOldList();
			
			// candidate selection
			users.parallelStream().forEach(u -> hyrecCandidateSelection(u));
			
			tcs += (System.nanoTime() - tcs1);
			
			users.parallelStream().forEach(u -> clusteringHyrec(u));
//			
//			sim.runTask(new ClusteringHyrecFactoryV2(mapping));
//			

			if(log==true) {	
				
				Tools.log(resultsPath, "\n"+iteration + " ");
				System.out.println(iteration);
				
				Measures.recallMeasure(mapping, resultsPath, sizeknn, best);
				Measures.nbSimilarityEvaluationMeasure(mapping, resultsPath);
				Measures.nbUpdateMeasure(mapping, resultsPath, terminationThreshold);			
			}
			
			if(terminaison(terminationThreshold)) {				
				break;
			}	
			
			if(iteration==cycleTerminaison) {
				break;
			}
			
			iteration++;
		}
		
		long tprocessing = System.nanoTime();
				
		
		System.out.println("iteration " + iteration);
		
		long readProfile =tprofile2-tprofile1;
		long computation = (tprocessing-tpreproc2);
		long tpreproc =tpreproc2-tpreproc1;
		long global = tprocessing-tprofile1;

		System.out.println("time profile construction " + TimeUnit.MILLISECONDS.convert(readProfile, TimeUnit.NANOSECONDS));
		System.out.println("time preproc " + TimeUnit.MILLISECONDS.convert(tpreproc, TimeUnit.NANOSECONDS));
		System.out.println("time candidate selection " + TimeUnit.MILLISECONDS.convert(tcs, TimeUnit.NANOSECONDS));
		System.out.println("time computation " + TimeUnit.MILLISECONDS.convert(computation, TimeUnit.NANOSECONDS));
		System.out.println("time global " + TimeUnit.MILLISECONDS.convert(global, TimeUnit.NANOSECONDS));
		
		Tools.log(resultsPath, "\ntime profile construction " + TimeUnit.MILLISECONDS.convert(readProfile, TimeUnit.NANOSECONDS));
		Tools.log(resultsPath, "\ntime preproc " + TimeUnit.MILLISECONDS.convert(tpreproc, TimeUnit.NANOSECONDS));
		Tools.log(resultsPath, "\ntime candidate selection " + TimeUnit.MILLISECONDS.convert(tcs, TimeUnit.NANOSECONDS));
		Tools.log(resultsPath, "\ntime computation " + TimeUnit.MILLISECONDS.convert(computation, TimeUnit.NANOSECONDS));
		Tools.log(resultsPath, "\ntime global " + TimeUnit.MILLISECONDS.convert(global, TimeUnit.NANOSECONDS)+"\n");
		
		if(!log) {
			best = Tools.loadBest(bestMatchPath+".object");
		}
		Measures.recallMeasure(mapping, resultsPath, sizeknn, best);
		Measures.nbSimilarityEvaluationMeasure(mapping, resultsPath);
		Measures.nbUpdateMeasure(mapping, resultsPath, terminationThreshold);	
		
	}
	

	private void clusteringHyrec(Integer uid) {
		
		double note;
		
		MyPeerHyRec me = (MyPeerHyRec) mapping.get(uid);
		
		// new incoming node with new and old
		for(Integer u1 : me.get_newIncomming()) {
			
			for(Integer u2 : me.get_oldOutComing()) {
								
				if(u1.compareTo(u2)!=0) {
					
					// compute the distance
					note = mapping.get(u1).getMyProfile().getCos(mapping.get(u2).getMyProfile());
					me.incNbSimilarityEvaluation();
							
					boolean inserted =  mapping.get(u1).updateNeighborhood(u2, note);
					if(inserted) {
						mapping.get(u1).incUpdate();
					}
				
					inserted =  mapping.get(u2).updateNeighborhood(u1, note);
					if(inserted) {						
						mapping.get(u2).incUpdate();						
					}
				}
			}
			
			for(Integer u2 : me.get_newOutcoming()) {
				
				if(u1.compareTo(u2)!=0) {

					// compute the distance
					note = mapping.get(u1).getMyProfile().getCos(mapping.get(u2).getMyProfile());

					me.incNbSimilarityEvaluation();
										
					boolean inserted =  mapping.get(u1).updateNeighborhood(u2, note);
					if(inserted) {
						mapping.get(u1).incUpdate();
					}
				
					inserted =  mapping.get(u2).updateNeighborhood(u1, note);
					if(inserted) {						
						mapping.get(u2).incUpdate();						
					}
				}
			}
			
		}
		

		// old incoming node with new
		for(Integer u1 : me.get_oldIncomming()) {
			
			for(Integer u2 : me.get_newOutcoming()) {
				
				if(u1.compareTo(u2)!=0) {

					// compute the distance
					note = mapping.get(u1).getMyProfile().getCos(mapping.get(u2).getMyProfile());

					me.incNbSimilarityEvaluation();
													
					boolean inserted =  mapping.get(u1).updateNeighborhood(u2, note);
					if(inserted) {
						mapping.get(u1).incUpdate();
					}
				
					inserted =  mapping.get(u2).updateNeighborhood(u1, note);
					if(inserted) {						
						mapping.get(u2).incUpdate();						
					}
				}
			}
		}
		
		// random
		for(Integer u1 : me.get_random()) {
			
			if(u1.compareTo(uid)!=0) {

				// compute the distance
				note = mapping.get(uid).getMyProfile().getCos(mapping.get(u1).getMyProfile());

				me.incNbSimilarityEvaluation();
													
				boolean inserted =  mapping.get(uid).updateNeighborhood(u1, note);
				if(inserted) {
					mapping.get(uid).incUpdate();
				}
					
				mapping.get(u1).updateNeighborhood(uid, note);
				if(inserted) {						
					mapping.get(u1).incUpdate();						
				}	
			}
			
		}		
		
		me.getCandidates().clear();

		me.incIteration();
	}


	private void hyrecCandidateSelection(Integer uid) {
//		int sizeknn2, int sizeRandom2, List<Integer> users2, ConcurrentHashMap<Integer, MyPeer> mapping2) {
//	}
		
		MyPeerHyRec me = (MyPeerHyRec) mapping.get(uid);
		
		me.cleanUpdate();		
		me.getCandidates().clear();
		me.get_random().clear();
		
		// candidate = new + newp + old + oldp + random
		
		for(Integer id : me.get_newIncomming()) {
			me.getCandidates().add(id);		
		}
		

		for(Integer id : me.get_newOutcoming()) {
							
			if(!me.getCandidates().contains(id)) {
				me.getCandidates().add(id);
			}
		}
				
		for(Integer id : me.get_oldIncomming()) {
			
			if(!me.getCandidates().contains(id)) {
				me.getCandidates().add(id);
			}
		}
		
		for(Integer id : me.get_oldOutComing()) {
			
			if(!me.getCandidates().contains(id)) {
				me.getCandidates().add(id);
			}
		}
		
		
		// add random peers (not mandatory / avoid to loop until to find a random and unknown peer)
		for(int i=0; i<sizeRandom; i++) {
			
			Integer pid = users.get(me.getRandom().nextInt(users.size()));

			if(pid.compareTo(uid)!=0) {
				
				me.get_random().add(pid);		
				
				if(!me.getCandidates().contains(pid)) {
					
					me.getCandidates().add(pid);
				}				
			}			
		}		
		
	}


	private void updateNewOldList(Integer uid) {
		
		MyPeerNNDesc me = (MyPeerNNDesc)mapping.get(uid);
		
		me.get_newIncomming().clear();
		me.get_newOutcoming().clear();
		me.get_oldIncomming().clear();
		me.get_oldOutComing().clear();
			
		for(int i=0; i<mapping.get(uid).getTopk().length; i++) {
			
			// flag is false
			if(!me.getFlags()[i]) {
				me.get_oldOutComing().add(me.getTopk()[i]);
			}
			// flag is true
	         else {	        	 
	        	 me.get_newOutcoming().add(me.getTopk()[i]);
	        	 me.getFlags()[i] = false;        	 
	         }
		}
	}


	private void initKNNRandomly(Integer uid) {
		
		ArrayList<Integer> currentList = new ArrayList<Integer>();
		for(int i=0; i<mapping.get(uid).getTopk().length; i++) {
			currentList.add(mapping.get(uid).getTopk()[i]);
		}
				
		while(mapping.get(uid).getSizeNeighborhood()<sizeknn) {
			
			Integer id = users.get(mapping.get(uid).getRandom().nextInt(users.size()));
			
			if(id!=uid && !currentList.contains(id)) {
				
				mapping.get(uid).updateNeighborhood(id, 0);
				currentList.add(id);
			}
		}	
		
		// HyRec nodes extands NNDescent nodes 
		((MyPeerNNDesc)mapping.get(uid)).set_newIncomming(new TreeSet<Integer>());
		((MyPeerNNDesc)mapping.get(uid)).set_oldIncomming(new TreeSet<Integer>());
		((MyPeerNNDesc)mapping.get(uid)).set_newOutcoming(new TreeSet<Integer>());
		((MyPeerNNDesc)mapping.get(uid)).set_oldOutComing(new TreeSet<Integer>());		
		((MyPeerHyRec)mapping.get(uid)).set_random(new TreeSet<Integer>());
	}


	private static boolean terminaison(double t) {
		
		double nb=0;
		
		for (Integer uid : mapping.keySet()) {
			
			nb += mapping.get(uid).getUpdate();		
			
		}
				
		nb /= mapping.size();
		
		if(nb <= t) {				
			return true;
		}	
		
		return false;
	}
	
	public static Hashtable<Integer, ArrayList<Integer>> getKNNGraph() {
		
		Hashtable<Integer, ArrayList<Integer>> neighborhoodGraph = new Hashtable<Integer, ArrayList<Integer>>();
		
		for(Integer uid : userProfiles.keySet()) {
			neighborhoodGraph.put(uid, new ArrayList<Integer>());
			for(int i=0; i<mapping.get(uid).getTopk().length; i++) {
				neighborhoodGraph.get(uid).add(mapping.get(uid).getTopk()[i]);
			}
//			neighborhoodGraph.put(uid, new ArrayList<Integer>(mapping.get(uid).getHeapFriends().getElements()));
		}
		
		return neighborhoodGraph;
	}
	
	public static Hashtable<Integer, ScoreCount<Integer>> getKNNValueGraph() {
		
		Hashtable<Integer, ScoreCount<Integer>> n = new Hashtable<Integer, ScoreCount<Integer>>();
		
		for(Integer uid : userProfiles.keySet()) {
			n.put(uid, new ScoreCount<Integer>());
			
			for(int i=0; i<mapping.get(uid).getTopk().length; i++) {
				
				n.get(uid).addValue(mapping.get(uid).getTopk()[i], mapping.get(uid).getScores()[i]);
			}
			
//			Iterator<HeapNode> it = mapping.get(uid).getHeapFriends().getSortedHeap().iterator();
//			while (it.hasNext()) {
//				
//				HeapNode type = (HeapNode) it.next();
//				
//				n.get(uid).addValue(type.getId(), type.getScore());
//				
//			}
		}
		
		return n;
	}
		

	private static void loadDataset(int dataset) {
		

		if(dataset==-1) {
			
			resultsPath += "ml2-1-ratings-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-1-ratings-k"+sizeknn;
			
			userProfiles = Tools.loadProfile("ml2-ratings1000209.object");
			 
		}
		else if(dataset==-2) {
			
			resultsPath += "ml2-2-ratings-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-2-ratings-k"+sizeknn;
			
			userProfiles = Tools.loadProfile("ml2-ratings500009.object");

			 
		}
		else if(dataset==-3) {
			
			resultsPath += "ml2-3-ratings-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-3-ratings-k"+sizeknn;
			
			userProfiles = Tools.loadProfile("ml2-ratings255188.object");

			 
		}
		else if(dataset==-4) {
			
			resultsPath += "ml2-4-ratings-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-4-ratings-k"+sizeknn;
			
			userProfiles = Tools.loadProfile("ml2-ratings131668.object");

			 
		}
		else if(dataset==-5) {
			
			resultsPath += "ml2-5-ratings-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-5-ratings-k"+sizeknn;
			
			userProfiles = Tools.loadProfile("ml2-ratings68415.object");

			 
		}
		else if(dataset==0) {
			
			resultsPath += "ml1-k"+sizeknn;
			bestMatchPath += "bestKNN-ml1-k"+sizeknn;
			ProfileLoader.UsersStarValueProfileLoader("dataset/ml-100k/u.data", userProfiles);
		}
		else if(dataset==1) {
			
			resultsPath += "ml2-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-k"+sizeknn;
			ProfileLoader.UsersStarValueProfileLoader("dataset/ml-1m/ratings.dat", userProfiles);			
		}
		else if(dataset==2) {
			
			resultsPath += "arxiv1-k"+sizeknn;	
			bestMatchPath += "bestKNN-arxiv1-k"+sizeknn;
			ProfileLoader.UsersProfileLoader("dataset/Arxiv/CA-GrQc.txt", userProfiles);
		}
		else if(dataset==3) {
			
			resultsPath += "arxiv2-k"+sizeknn+"-t"+terminationThreshold;	
			bestMatchPath += "bestKNN-arxiv2-k"+sizeknn;
			ProfileLoader.UsersProfileLoader("dataset/Arxiv/CA-AstroPh.txt", userProfiles);
		}
		else if(dataset==4) {
			
			resultsPath += "wikipedia-k"+sizeknn+"-t"+terminationThreshold;	
			bestMatchPath += "bestKNN-wikipedia-k"+sizeknn;
			ProfileLoader.UsersProfileLoader("dataset/wikipedia/Wiki-Vote.txt", userProfiles);
		}
		else if(dataset==5) {
			
			resultsPath += "gowalla-k"+sizeknn+"-t"+terminationThreshold;
			bestMatchPath += "bestKNN-gowalla-k"+sizeknn;
			ProfileLoader.UsersValueProfileLoader("dataset/loc-gowalla/Gowalla_totalCheckins.txt-value", userProfiles);
		
		}
		else if(dataset==6) {
			
			resultsPath += "dblp-k"+sizeknn+"-t"+terminationThreshold;
			bestMatchPath += "bestKNN-dblp-k"+sizeknn;
			ProfileLoader.UsersValueProfileLoader("dataset/dblp/dblp.xml.value.clean.filter5.txt", userProfiles);
		}
		
	}
	
	private static boolean terminaisonItemBasedCandidateSetSize() {
		
		for (Integer uid : mapping.keySet()) {
			
			MyPeerFKNN p1 = (MyPeerFKNN)mapping.get(uid);
			
			if(p1.getItemBasedCandidates().size()>0) {
				return false;
			}	
		}
		
		return true;
	}
	

	private static void reverseNewOldList() {
		
		for(Integer uid : mapping.keySet()) {
			
			for(Integer fid : ((MyPeerNNDesc)mapping.get(uid)).get_newOutcoming()) {
				
				((MyPeerNNDesc)mapping.get(fid)).get_newIncomming().add(uid);				
			}
			
			for(Integer fid : ((MyPeerNNDesc)mapping.get(uid)).get_oldOutComing()) {
				
				((MyPeerNNDesc)mapping.get(fid)).get_oldIncomming().add(uid);
			}
		}
	}
	
	
	
	public static void main(String[] args) {
		
//		if(args.length<8) {
//			System.out.println("USAGE: {dataset 3:arxiv 4:wikipedia 5:gowalla 6:dbpedia} "
//					+ "k random cycleTermination thresholdTermination log(boolean) resultsPath bestPath");
//			System.exit(0);
//		}
//		
//		int dataset = Integer.parseInt(args[0]);			// dataset
//		int k = Integer.parseInt(args[1]);					// size knn
//		int sr = Integer.parseInt(args[2]);					// size random
//		int c = Integer.parseInt(args[3]);					// cycleTerminaison
//		double t = Double.parseDouble(args[4]);				//  terminationThreshold//		
//		Boolean l = Boolean.parseBoolean(args[5]); 			// log
//		String r = args[6]; 								// resultsPath
//		String bestKNNPath = args[7];						// string best knn (for recall measurement)
		
		
		int dataset = 4;					// dataset
		int k = 20;							// size knn
		int sr = 20;							// size random
		int c = 100; 						// cycleTerminaison		
		double t = 0.001; 					//  terminationThreshold		
		Boolean l = false; 					// log
		String r = "results/hyrec-"; 		// resultsPath
		String bestKNNPath = "bestKNN/";	// string best knn (for recall measurement)
		
		
		HYREC hyrec = new HYREC(k, dataset, t, r, c, l, bestKNNPath, sr);
		
	}
}




