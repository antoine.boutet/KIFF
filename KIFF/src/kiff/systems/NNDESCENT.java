package kiff.systems;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Map;
import java.util.Random;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import gnu.trove.map.hash.TIntDoubleHashMap;
import kiff.util.MyPeerNNDesc;
import kiff.util.ProfileLoader;
import kiff.util.ScoreCount;
import kiff.util.Measures;
import kiff.util.MyPeer;
import kiff.util.MyPeerFKNN;
import kiff.util.MyPeerHyRec;
import kiff.util.Tools;

public class NNDESCENT {
	
	private static Random random = new Random();
	
	public static Map<Integer, ScoreCount<Integer>> userProfiles;
	public static ArrayList<Integer> users;
	
	public static ConcurrentHashMap<Integer, MyPeer> mapping;
	public static Map<Integer, TIntDoubleHashMap> best;
	
	private static String resultsPath;
	private static int sizeknn;	
	private static int cycleTerminaison;
	private static Boolean log; 
	private static double terminationThreshold;
	private static String bestMatchPath;

	public NNDESCENT(int k, 
			int dataset, 
			double t, 
			String r,
			int c,
			boolean l,
			String bs) {
		
		
		terminationThreshold = t;
		resultsPath= r;
		cycleTerminaison = c;
		log = l;
		bestMatchPath = bs;
		
		userProfiles = new Hashtable<Integer, ScoreCount<Integer>>();
		
		sizeknn = k;
		
		long tprofile1 = System.nanoTime();

		// build user and item profile
		loadDataset(dataset);
		
		users = new ArrayList<Integer>(userProfiles.keySet());
		
		long tprofile2 = System.nanoTime();
		
		mapping = new ConcurrentHashMap<Integer, MyPeer>();		
		
		for(Integer uid : userProfiles.keySet()) {
			MyPeer p = new MyPeerHyRec(uid, random, userProfiles.get(uid), sizeknn);
			mapping.put(uid, p);		
		}	
		
		long tpreproc1 = System.nanoTime();
		
		users.parallelStream().forEach(u -> initKNNRandomly(u));
		
		long tpreproc2 = System.nanoTime();
		
		if(log==true) {	
			
			best = Tools.loadBest(bestMatchPath+".object");				
			Measures.recallMeasure(mapping, resultsPath, sizeknn, best);		
		}
			
		int iteration = 0;
		
		long tcs = 0;
		
		while(true) {

			long tcs1 = System.nanoTime();

			// update listes
			users.parallelStream().forEach(u -> updateNewOldList(u));
			reverseNewOldList();			
			
			// candidate selection
			users.parallelStream().forEach(u -> dongCandidateSection(u));
			
			tcs += (System.nanoTime() - tcs1);
			

			users.parallelStream().forEach(u -> dongClustering(u));
			

			if(log==true) {	
				
				Tools.log(resultsPath, "\n"+iteration + " ");
				System.out.println(iteration);
				
				Measures.recallMeasure(mapping, resultsPath, sizeknn, best);
				Measures.nbSimilarityEvaluationMeasure(mapping, resultsPath);
				Measures.nbUpdateMeasure(mapping, resultsPath, terminationThreshold);
			}
			
			if(terminaison(terminationThreshold)) {				
				break;
			}	
			
			if(iteration==cycleTerminaison) {
				break;
			}
			
			iteration++;
		}
		
		long tprocessing = System.nanoTime();
				
		
		System.out.println("iteration " + iteration);
		
		long readProfile =tprofile2-tprofile1;
		long computation = (tprocessing-tpreproc2);
		long tpreproc =tpreproc2-tpreproc1;
		long global = tprocessing-tprofile1;

		System.out.println("time profile construction " + TimeUnit.MILLISECONDS.convert(readProfile, TimeUnit.NANOSECONDS));
		System.out.println("time preproc " + TimeUnit.MILLISECONDS.convert(tpreproc, TimeUnit.NANOSECONDS));
		System.out.println("time candidate selection " + TimeUnit.MILLISECONDS.convert(tcs, TimeUnit.NANOSECONDS));
		System.out.println("time computation " + TimeUnit.MILLISECONDS.convert(computation, TimeUnit.NANOSECONDS));
		System.out.println("time global " + TimeUnit.MILLISECONDS.convert(global, TimeUnit.NANOSECONDS));
		
		Tools.log(resultsPath, "\ntime profile construction " + TimeUnit.MILLISECONDS.convert(readProfile, TimeUnit.NANOSECONDS));
		Tools.log(resultsPath, "\ntime preproc " + TimeUnit.MILLISECONDS.convert(tpreproc, TimeUnit.NANOSECONDS));
		Tools.log(resultsPath, "\ntime candidate selection " + TimeUnit.MILLISECONDS.convert(tcs, TimeUnit.NANOSECONDS));
		Tools.log(resultsPath, "\ntime computation " + TimeUnit.MILLISECONDS.convert(computation, TimeUnit.NANOSECONDS));
		Tools.log(resultsPath, "\ntime global " + TimeUnit.MILLISECONDS.convert(global, TimeUnit.NANOSECONDS)+"\n");
		
		if(!log) {
			best = Tools.loadBest(bestMatchPath+".object");
		}		
		Measures.recallMeasure(mapping, resultsPath, sizeknn, best);
		Measures.nbSimilarityEvaluationMeasure(mapping, resultsPath);
		Measures.nbUpdateMeasure(mapping, resultsPath, terminationThreshold);		
		
	}
	
	private void updateNewOldList(Integer uid) {

		MyPeerNNDesc me = (MyPeerNNDesc)mapping.get(uid);
		
		me.get_newIncomming().clear();
		me.get_newOutcoming().clear();
		me.get_oldIncomming().clear();
		me.get_oldOutComing().clear();
			
		for(int i=0; i<me.getTopk().length; i++) {
			
			// flag is false
			if(!me.getFlags()[i]) {
				me.get_oldOutComing().add(me.getTopk()[i]);
			}
			// flag is true
	         else {	        	 
	        	 me.get_newOutcoming().add(me.getTopk()[i]);
	        	 me.getFlags()[i] = false;        	 
	         }
		}
	}


	private void dongClustering(Integer uid) {
		
		double note;
		
		MyPeerNNDesc me = (MyPeerNNDesc) mapping.get(uid);

		for(Integer u1 : me.get_newOutcoming()) {
			
			for(Integer u2 : me.get_newOutcoming()) {
				
				if(u1 < u2) {
					

					// compute the distance
					note = mapping.get(u1).getMyProfile().getCos(mapping.get(u2).getMyProfile());
					me.incNbSimilarityEvaluation();
								
					boolean inserted =  mapping.get(u1).updateNeighborhood(u2, note);
					if(inserted) {
						mapping.get(u1).incUpdate();
					}
				
					inserted = mapping.get(u2).updateNeighborhood(u1, note);
					if(inserted) {						
						mapping.get(u2).incUpdate();						
					}
				}
			}
			
			for(Integer u2 : me.get_oldOutComing()) {
				
				if(me.get_newOutcoming().contains(u2)) {
					continue;
				}
				
				// compute the distance
				note = mapping.get(u1).getMyProfile().getCos(mapping.get(u2).getMyProfile());
				me.incNbSimilarityEvaluation();
								
				boolean inserted =  mapping.get(u1).updateNeighborhood(u2, note);
				if(inserted) {
					mapping.get(u1).incUpdate();	
					
				}
			
				inserted = mapping.get(u2).updateNeighborhood(u1, note);
				if(inserted) {						
					mapping.get(u2).incUpdate();	
					
				}
			}	
		}
		
		me.incIteration();
	}


	private void dongCandidateSection(Integer uid) {
		
		MyPeerNNDesc me = (MyPeerNNDesc)mapping.get(uid);
		
		me.cleanUpdate();
		me.getCandidates().clear();
		
		// Union of list
		
		int counter=0;
		
		ArrayList<Integer> list = new ArrayList<Integer>(me.get_oldIncomming());
		Collections.shuffle(list);
		while(counter<sizeknn && list.size()>0) {
			me.get_oldOutComing().add(list.get(0));
			list.remove(0);
			counter++;
		}
		
		
		counter=0;
		
		list = new ArrayList<Integer>(me.get_newIncomming());
		Collections.shuffle(list);
		while(counter<sizeknn && list.size()>0) {
			me.get_newOutcoming().add(list.get(0));
			list.remove(0);
			counter++;
		}
		

		for(Integer id : me.get_newOutcoming()) {			
			me.getCandidates().add(id);			
		}
		
		for(Integer id : me.get_oldOutComing()) {
			
			if(!me.getCandidates().contains(id)) {
				me.getCandidates().add(id);
			}			
		}
		
	}


	private void initKNNRandomly(Integer uid) {
		

		ArrayList<Integer> currentList = new ArrayList<Integer>();
		
		while(mapping.get(uid).getSizeNeighborhood()<sizeknn) {
			
			Integer id = users.get(mapping.get(uid).getRandom().nextInt(users.size()));
			
			if(id!=uid && !currentList.contains(id)) {
				
				((MyPeerNNDesc)mapping.get(uid)).updateNeighborhood(id, 0);
				currentList.add(id);
			}
		}	
		
		((MyPeerNNDesc)mapping.get(uid)).set_newIncomming(new TreeSet<Integer>());
		((MyPeerNNDesc)mapping.get(uid)).set_oldIncomming(new TreeSet<Integer>());
		((MyPeerNNDesc)mapping.get(uid)).set_newOutcoming(new TreeSet<Integer>());
		((MyPeerNNDesc)mapping.get(uid)).set_oldOutComing(new TreeSet<Integer>());		
	}


	private static boolean terminaison(double t) {
		
		double nb=0;
		
		for (Integer uid : mapping.keySet()) {
			nb += mapping.get(uid).getUpdate();		
		}
				
		nb /= mapping.size();
		
		if(nb <= t) {				
			return true;
		}	
		
		return false;
	}
	
	public static Hashtable<Integer, ArrayList<Integer>> getKNNGraph() {
		
		Hashtable<Integer, ArrayList<Integer>> neighborhoodGraph = new Hashtable<Integer, ArrayList<Integer>>();
		
		for(Integer uid : userProfiles.keySet()) {
			neighborhoodGraph.put(uid, new ArrayList<Integer>());
			for(int i=0; i<mapping.get(uid).getTopk().length; i++) {
				neighborhoodGraph.get(uid).add(mapping.get(uid).getTopk()[i]);
			}
//			neighborhoodGraph.put(uid, new ArrayList<Integer>(mapping.get(uid).getHeapFriends().getElements()));
		}
		
		return neighborhoodGraph;
	}
	
	public static Hashtable<Integer, ScoreCount<Integer>> getKNNValueGraph() {
		
		Hashtable<Integer, ScoreCount<Integer>> n = new Hashtable<Integer, ScoreCount<Integer>>();
		
		for(Integer uid : userProfiles.keySet()) {
			n.put(uid, new ScoreCount<Integer>());
			
			for(int i=0; i<mapping.get(uid).getTopk().length; i++) {
				
				n.get(uid).addValue(mapping.get(uid).getTopk()[i], mapping.get(uid).getScores()[i]);
			}
			
//			Iterator<HeapNode> it = mapping.get(uid).getHeapFriends().getSortedHeap().iterator();
//			while (it.hasNext()) {
//				
//				HeapNode type = (HeapNode) it.next();
//				
//				n.get(uid).addValue(type.getId(), type.getScore());
//				
//			}
		}
		
		return n;
	}
		

	private static void loadDataset(int dataset) {
		

		if(dataset==-1) {
			
			resultsPath += "ml2-1-ratings-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-1-ratings-k"+sizeknn;
			
			userProfiles = Tools.loadProfile("ml2-ratings1000209.object");
			 
		}
		else if(dataset==-2) {
			
			resultsPath += "ml2-2-ratings-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-2-ratings-k"+sizeknn;
			
			userProfiles = Tools.loadProfile("ml2-ratings500009.object");

			 
		}
		else if(dataset==-3) {
			
			resultsPath += "ml2-3-ratings-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-3-ratings-k"+sizeknn;
			
			userProfiles = Tools.loadProfile("ml2-ratings255188.object");

			 
		}
		else if(dataset==-4) {
			
			resultsPath += "ml2-4-ratings-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-4-ratings-k"+sizeknn;
			
			userProfiles = Tools.loadProfile("ml2-ratings131668.object");

			 
		}
		else if(dataset==-5) {
			
			resultsPath += "ml2-5-ratings-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-5-ratings-k"+sizeknn;
			
			userProfiles = Tools.loadProfile("ml2-ratings68415.object");

			 
		}
		else if(dataset==0) {
			
			resultsPath += "ml1-k"+sizeknn;
			bestMatchPath += "bestKNN-ml1-k"+sizeknn;
			ProfileLoader.UsersStarValueProfileLoader("dataset/ml-100k/u.data", userProfiles);
		}
		else if(dataset==1) {
			
			resultsPath += "ml2-k"+sizeknn;
			bestMatchPath += "bestKNN-ml2-k"+sizeknn;
			ProfileLoader.UsersStarValueProfileLoader("dataset/ml-1m/ratings.dat", userProfiles);			
		}
		else if(dataset==2) {
			
			resultsPath += "arxiv1-k"+sizeknn;	
			bestMatchPath += "bestKNN-arxiv1-k"+sizeknn;
			ProfileLoader.UsersProfileLoader("dataset/Arxiv/CA-GrQc.txt", userProfiles);
		}
		else if(dataset==3) {
			
			resultsPath += "arxiv2-k"+sizeknn+"-t"+terminationThreshold;	
			bestMatchPath += "bestKNN-arxiv2-k"+sizeknn;
			ProfileLoader.UsersProfileLoader("dataset/Arxiv/CA-AstroPh.txt", userProfiles);
		}
		else if(dataset==4) {
			
			resultsPath += "wikipedia-k"+sizeknn+"-t"+terminationThreshold;	
			bestMatchPath += "bestKNN-wikipedia-k"+sizeknn;
			ProfileLoader.UsersProfileLoader("dataset/wikipedia/Wiki-Vote.txt", userProfiles);
		}
		else if(dataset==5) {
			
			resultsPath += "gowalla-k"+sizeknn+"-t"+terminationThreshold;
			bestMatchPath += "bestKNN-gowalla-k"+sizeknn;
			ProfileLoader.UsersValueProfileLoader("dataset/loc-gowalla/Gowalla_totalCheckins.txt-value", userProfiles);
		
		}
		else if(dataset==6) {
			
			resultsPath += "dblp-k"+sizeknn+"-t"+terminationThreshold;
			bestMatchPath += "bestKNN-dblp-k"+sizeknn;
			ProfileLoader.UsersValueProfileLoader("dataset/dblp/dblp.xml.value.clean.filter5.txt", userProfiles);
		}
		
	}
	
	private static boolean terminaisonItemBasedCandidateSetSize() {
		
		for (Integer uid : mapping.keySet()) {
			
			MyPeerFKNN p1 = (MyPeerFKNN)mapping.get(uid);
			
			if(p1.getItemBasedCandidates().size()>0) {
				return false;
			}	
		}
		
		return true;
	}
	

	private static void reverseNewOldList() {
		
		for(Integer uid : mapping.keySet()) {
			
			for(Integer fid : ((MyPeerNNDesc)mapping.get(uid)).get_newOutcoming()) {
				
				((MyPeerNNDesc)mapping.get(fid)).get_newIncomming().add(uid);				
			}
			
			for(Integer fid : ((MyPeerNNDesc)mapping.get(uid)).get_oldOutComing()) {
				
				((MyPeerNNDesc)mapping.get(fid)).get_oldIncomming().add(uid);
			}
		}
	}
	
	
	
	public static void main(String[] args) {
		
//		if(args.length<7) {
//			System.out.println("USAGE: {dataset 3:arxiv 4:wikipedia 5:gowalla 6:dbpedia} "
//					+ "k cycleTermination thresholdTermination log(boolean) resultsPath bestPath");
//			System.exit(0);
//		}
//		
//		int dataset = Integer.parseInt(args[0]);			// dataset
//		int k = Integer.parseInt(args[1]);					// size knn
//		int c = Integer.parseInt(args[2]);					// cycleTerminaison
//		double t = Double.parseDouble(args[3]);				//  terminationThreshold//		
//		Boolean l = Boolean.parseBoolean(args[4]); 			// log
//		String r = args[5]; 								// resultsPath
//		String bestKNNPath = args[6];						// string best knn (for recall measurement)
		
		
		
		int dataset = 3;					// dataset
		int k = 20;							// size knn
		int c = 100; 						// cycleTerminaison		
		double t = 0.001; 					//  terminationThreshold		
		Boolean l = false; 					// log
		String r = "results/nndescent-"; 		// resultsPath
		String bestKNNPath = "bestKNN/";	// string best knn (for recall measurement)
		
		
		NNDESCENT nnDescent = new NNDESCENT(k, dataset, t, r, c, l, bestKNNPath);
		
	}
}




