package kiff.util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Random;
import java.util.Set;

import kiff.util.ScoreCount;



public class MyPeerHyRec extends MyPeerNNDesc implements Serializable  {
	
	private Set<Integer> _random;
	
	public MyPeerHyRec(int id, Random r, ScoreCount<Integer> p, int sizeKnn) {		
		super(id,r,p,sizeKnn);		
	}
	

	 private void writeObject(ObjectOutputStream out) throws IOException {
			
//			out.writeObject(this.hops);
//			out.writeObject(this.targets);
//			out.writeObject(this.rumorScoreCount);
		}
	    
		private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
			
//			this.hops = (ArrayList<Hop>) in.readObject();
//			this.targets = (ArrayList<Integer>) in.readObject();
//			this.rumorScoreCount = (ScoreCount<Integer>) in.readObject();
		}

		public Set<Integer> get_random() {
			return _random;
		}


		public void set_random(Set<Integer> _random) {
			this._random = _random;
		}
}

