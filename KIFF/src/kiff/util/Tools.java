package kiff.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import gnu.trove.map.hash.TIntDoubleHashMap;
import kiff.util.ScoreCount;

/**
 * lib of useful methods
 * 
 * @author aboutet
 *
 */
public class Tools {
	
	
	public Tools() {}
	
	/**
	 * append to log file
	 * 
	 * @param filename
	 * @param message
	 */
	public static void log(String filename, String message) {
		
		try{
			// Create file
			FileWriter fstream = new FileWriter(filename, true);
			BufferedWriter out = new BufferedWriter(fstream);
			
			out.write(message);			
			
			out.close();
			
		}
		catch (Exception e){
			//Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
		
	}
	
	/**
	 * Save object to file
	 * 
	 * @param s
	 * @param o
	 */
	public static void saveObject(String s, Object o) {
		try {
			FileOutputStream fos = new FileOutputStream(s+".object");
			ObjectOutputStream oos = new ObjectOutputStream(fos);

			oos.writeObject(o);
			oos.flush();
			fos.close();
		} catch(Exception e) {
		}
		
		System.out.println("Object saved ... " + s+".object");
	}
	
	
	
	public static void saveGraphValue(String filename, Map<Integer, TIntDoubleHashMap> g) {
		
		try{
			// Create file
			FileWriter fstream = new FileWriter(filename);
			BufferedWriter out = new BufferedWriter(fstream);
			
			for(Integer uid : g.keySet()) {
				
				for(int fid : g.get(uid).keys()) {
					out.write(uid + " " + fid + " " + g.get(uid).get(fid) + "\n");
				}
				
			}
			
			out.close();
			
		}
		catch (Exception e){
			//Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}
	
	/**
	 * write the graph in a text file
	 * 
	 * @param filename
	 * @param g
	 */
	public static void saveGraph(String filename, Hashtable<Integer, ArrayList<Integer>> g) {
		
		try{
			// Create file
			FileWriter fstream = new FileWriter(filename);
			BufferedWriter out = new BufferedWriter(fstream);
			
			for(Integer uid : g.keySet()) {
				
				out.write(uid + " ");
				
				for(Integer fid : g.get(uid)) {
					out.write(fid + " ");
				}
				
				out.write("\n");
			}
			
			out.close();
			
		}
		catch (Exception e){
			//Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}
	
	/**
	 * write the graph in a text file
	 * 
	 * @param filename
	 * @param mapping
	 */
	public static void saveGraphScore(String filename, Hashtable<Integer, MyPeer> mapping) {
		
		try{
			// Create file
			FileWriter fstream = new FileWriter(filename);
			BufferedWriter out = new BufferedWriter(fstream);
			
			for(Integer uid : mapping.keySet()) {
				
				out.write(uid + " ");
				
				for(int i=0; i<mapping.get(uid).getTopk().length; i++) {
					out.write(mapping.get(uid).getTopk()[i] +":"+ mapping.get(uid).getScores()[i] + " ");
				}
				
//				for(HeapNode fid : mapping.get(uid).getHeapFriends().getSortedHeap()) {
//					out.write(fid.getId() +":"+ fid.getScore() + " ");
//				}
				
				out.write("\n");
			}
			
			out.close();
			
		}
		catch (Exception e){
			System.err.println("Error: " + e.getMessage());
		}
	}
	
	/**
	 * read graph from object file
	 * 
	 * @param f
	 * @return
	 */
	public static Hashtable<Integer, ArrayList<Integer>> loadGraph(String f) {

//		System.out.println("Load the object: ");
		
		Hashtable<Integer, ArrayList<Integer>> g = null;
				
		try {
			FileInputStream fos = new FileInputStream(f);
			ObjectInputStream oos = new ObjectInputStream(fos);
		
			g = (Hashtable<Integer, ArrayList<Integer>>) oos.readObject();
			
//			System.out.println("Load the graph: " + p);
			fos.close();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return g;
	}
	
	public static Hashtable<Integer, TIntDoubleHashMap> loadBest(String f) {

//		System.out.println("Load the object: ");
		
		Hashtable<Integer, TIntDoubleHashMap> g = null;
				
		try {
			FileInputStream fos = new FileInputStream(f);
			ObjectInputStream oos = new ObjectInputStream(fos);
		
			g = (Hashtable<Integer, TIntDoubleHashMap>)oos.readObject();
			
//			System.out.println("Load the graph: " + p);
			fos.close();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return g;
	}
	

	public static Hashtable<Integer, ScoreCount<Integer>> loadProfile(String f) {

//		System.out.println("Load the object: ");
		
		Hashtable<Integer, ScoreCount<Integer>> g = null;
				
		try {
			FileInputStream fos = new FileInputStream(f);
			ObjectInputStream oos = new ObjectInputStream(fos);
		
			g = (Hashtable<Integer, ScoreCount<Integer>>)oos.readObject();
			
//			System.out.println("Load the graph: " + p);
			fos.close();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return g;
	}
	
	/**
	 * Read graph from text file
	 * 
	 */

	public static Hashtable<Integer, ArrayList<Integer>> readBestKNN(String path) {
		
		Hashtable<Integer, ArrayList<Integer>> knn = new Hashtable<Integer, ArrayList<Integer>>();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			
			String line;
			while ((line = br.readLine()) != null) {
				
				
				Integer uid = Integer.parseInt(line.substring(0, line.indexOf(" ")));
				knn.put(uid, new ArrayList<Integer>());
				
				// TODO: while end of the line
				for(int i=0; i<10; i++) {
					line = line.substring(line.indexOf(" ")+1);
					Integer fid = Integer.parseInt(line.substring(0, line.indexOf(" ")));
//					System.out.println(fid);
					knn.get(uid).add(fid);
				}
			}
			
			br.close();						
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return knn;
	}
	

	/**
	 * Read graph from text file
	 * 
	 */

	public static Hashtable<Integer, Double> readBestKNNWithDistance(String path) {
		
		Hashtable<Integer, ScoreCount<Integer>> knn = new Hashtable<Integer, ScoreCount<Integer>>();
		Hashtable<Integer, Double> lowerBound = new Hashtable<Integer, Double>();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			
			String line;
			while ((line = br.readLine()) != null) {
				
				
				Integer uid = Integer.parseInt(line.substring(0, line.indexOf(" ")));
				knn.put(uid, new ScoreCount<Integer>());
//				System.out.println(uid);
				
				double lower = 1;
				
				for(int i=0; i<10; i++) {
					line = line.substring(line.indexOf(" ")+1);
					Integer fid = Integer.parseInt(line.substring(0, line.indexOf(":")));
					line = line.substring(line.indexOf(":")+1);
					Double d = Double.parseDouble(line.substring(0, line.indexOf(" ")));
					System.out.println(uid + " " + fid + " " + d);
					knn.get(uid).addValue(fid, d);
					
					if(lower>d) {
						lower=d;
					}
				}
				
				lowerBound.put(uid, lower);
				
				System.out.println(uid + " lower " + lower);
				
			}
			
			br.close();						
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return lowerBound;
	}

	public static Hashtable<Integer, Double> loadDistanceLowerBound(String f) {

		Hashtable<Integer, Double> g = null;
				
		try {
			FileInputStream fos = new FileInputStream(f);
			ObjectInputStream oos = new ObjectInputStream(fos);
		
			g = (Hashtable<Integer, Double>) oos.readObject();
			
//			System.out.println("Load the graph: " + p);
			fos.close();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return g;
	}

	// jaccard measure	
	public static Double jaccard(ScoreCount<Integer> sc1,
			ScoreCount<Integer> sc2) {

		double inter = 0;
		
		ScoreCount<Integer> union = new ScoreCount<Integer>();
		union.add(sc1);
		union.add(sc2);
		
		ArrayList<Integer> list2 = new ArrayList<Integer>(sc2.getItems());
		
		for(Integer rid : sc1.getItems()) {
			if(list2.contains(rid)) {
				inter++;
			}
		}
		
		double sizeUnion = union.size();
		union.clear();
		
//		System.out.println(inter / sizeUnion);
		return inter / sizeUnion;
	}
	

	// jaccard measure	
	public static Double l2(ScoreCount<Integer> sc1,
			ScoreCount<Integer> sc2) {

		double inter = 0;
		
		ScoreCount<Integer> union = new ScoreCount<Integer>();
		union.add(sc1);
		union.add(sc2);
		
		ArrayList<Integer> list2 = new ArrayList<Integer>(sc2.getItems());
		
		for(Integer rid : sc1.getItems()) {
			if(list2.contains(rid)) {
				inter++;
			}
		}
		
		double sizeUnion = union.size();
		union.clear();
		
//		System.out.println(inter / sizeUnion);
		return inter / sizeUnion;
	}


	// spearman measure	
	public static Double spearman(ScoreCount<Integer> sc1,
			ScoreCount<Integer> sc2) {
	
		int n = 0;
		double diff = 0;
		
		ArrayList<Integer> list2 = new ArrayList<Integer>(sc2.getItems());
		
		for(Integer rid : sc1.getItems()) {
			if(list2.contains(rid)) {
				n++;
				
				double d = sc1.getValue(rid) - sc2.getValue(rid);
				d *= d;
				
				diff += (d);
			}
		}
		
		if(n==0) {
			return 0.0;
		}
		
		if(n==1) {
			return 0.0;
		}
		
//		System.out.println("(n*((n*n)-1)) n " + n + " - "  + (n*((n*n)-1)));
		
		diff *= 6;
		diff /= (n*((n*n)-1));
		
//		System.out.println("diff " + diff);
		
		
//		diff = 1 - diff;
		
//
//		System.out.println("diff (1-) " + diff);
		
		return (1 - diff);
	}

	public static Double wup(ScoreCount<Integer> sc1,
			ScoreCount<Integer> sc2) {
		
		ScoreCount<Integer> projection = new ScoreCount<Integer>();
		
		ArrayList<Integer> list2 = new ArrayList<Integer>(sc2.getItems());
		
		for(Integer rid : sc1.getItems()) {
			if(list2.contains(rid)) {				
				projection.addValue(rid, sc1.getValue(rid));		
			}
		}
		
		return projection.getCos(sc2);
		
//		double numerator = sc1.getScalar(sc2);
//		
//		double norm = 0;
//		
//		ArrayList<Integer> list2 = new ArrayList<Integer>(sc2.getItems());
//		
//		for(Integer rid : sc1.getItems()) {
//			if(list2.contains(rid)) {				
//				norm += Math.pow(sc1.getValue(rid), 2);				
//			}
//		}
//		
//		norm = Math.sqrt(norm);
//		
//		norm *= sc2.getNorm();
//		
//		return numerator / norm;
	}
	

	public static Hashtable<Integer, ArrayList<Integer>> getKNNGraph(Hashtable<Integer, ScoreCount<Integer>> userProfiles,ConcurrentHashMap<Integer, MyPeer> mapping) {
		
		Hashtable<Integer, ArrayList<Integer>> neighborhoodGraph = new Hashtable<Integer, ArrayList<Integer>>();
		
		for(Integer uid : userProfiles.keySet()) {
			neighborhoodGraph.put(uid, new ArrayList<Integer>());
			for(int i=0; i<mapping.get(uid).getTopk().length; i++) {
				neighborhoodGraph.get(uid).add(mapping.get(uid).getTopk()[i]);
			}
//			neighborhoodGraph.put(uid, new ArrayList<Integer>(mapping.get(uid).getHeapFriends().getElements()));
		}
		
		return neighborhoodGraph;
	}
	
	public static Hashtable<Integer, TIntDoubleHashMap> getKNNValueGraph(Map<Integer, ScoreCount<Integer>> userProfiles,ConcurrentHashMap<Integer, MyPeer> mapping) {
		
		Hashtable<Integer, TIntDoubleHashMap> n = new Hashtable<Integer, TIntDoubleHashMap>();
		
		for(Integer uid : userProfiles.keySet()) {
			n.put(uid, new TIntDoubleHashMap());
			
			for(int i=0; i<mapping.get(uid).getTopk().length; i++) {
				
				if(mapping.get(uid).getScores()[i]>0) {
					n.get(uid).put(mapping.get(uid).getTopk()[i], mapping.get(uid).getScores()[i]);
				}
			}
		}
		
		return n;
	}

}
