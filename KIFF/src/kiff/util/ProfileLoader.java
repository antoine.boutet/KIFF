package kiff.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

import kiff.util.ScoreCount;

/**
 * Class to load profiles from files
 * 
 * @author aboutet
 *
 */
public class ProfileLoader {

//
//	/**
//	 * read 5 stars profile from file and define for each user if she liked / disliked items according to its average rating 
//	 * 
//	 * @param p
//	 * @param up
//	 * @param ip
//	 */
//	public static void UsersProfileRatingsLoader(String p, Hashtable<Integer, ScoreCount<Integer>> up) {
//		
//		// read file
//		try {
//			BufferedReader br = new BufferedReader(new FileReader(p));
//			
//			String line;
//			while ((line = br.readLine()) != null) {
//				Integer uid = Integer.parseInt(line.substring(0, line.indexOf("\t")));
//				if(!up.containsKey(uid)) {
//					up.put(uid, new ScoreCount<Integer>());
//				}
//				line = line.substring(line.indexOf("\t")+1);
//				Integer rid = Integer.parseInt(line.substring(0, line.indexOf("\t")));
//				
//				line = line.substring(line.indexOf("\t")+1);
//				Integer rating = Integer.parseInt(line.substring(0, line.indexOf("\t")));
//				
//				up.get(uid).addValue(rid, rating);
//			}
//			br.close();
//			
//			
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		
//	}
//	
//	/**
//	 * read profile (liked / dislike) from file
//	 * 
//	 * @param p
//	 * @param up
//	 * @param ip
//	 */
//	public static void UsersProfileLoader(String p, Hashtable<Integer, ScoreCount<Integer>> up) {
//		
//		try {
//			BufferedReader br = new BufferedReader(new FileReader(p));
//			
//			String line;
//			while ((line = br.readLine()) != null) {
//				
//				// skip the comment
//				if(line.contains("#")) {
//					continue;
//				}
//				
//				Integer uid = Integer.parseInt(line.substring(0, line.indexOf("\t")));
//				if(!up.containsKey(uid)) {
//					up.put(uid, new ScoreCount<Integer>());
//				}
//				line = line.substring(line.indexOf("\t")+1);
//				Integer rid = Integer.parseInt(line);
//				up.get(uid).addValue(rid,1);
//			}
//			br.close();
//			
//			
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
	

	/**
	 * read profile (liked / dislike) from file
	 * 
	 * @param p
	 * @param up
	 * @param ip
	 */
	public static void UsersItemsValueProfileLoader(String p, Map<Integer, ScoreCount<Integer>> up, Map<Integer, Set<Integer>> ip) {
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(p));
			
			String line;
			while ((line = br.readLine()) != null) {
				
				// skip the comment
				if(line.contains("#")) {
					continue;
				}
				
				Integer uid = Integer.parseInt(line.substring(0, line.indexOf(" ")));
				if(!up.containsKey(uid)) {
					up.put(uid, new ScoreCount<Integer>());
				}
				
				line = line.substring(line.indexOf(" ")+1);
				
				Integer rid = Integer.parseInt(line.substring(0, line.indexOf(" ")));
				
				if(!ip.containsKey(rid)) {
					Set<Integer> set = new HashSet<>();
					ip.put(rid, set);
				}
				ip.get(rid).add(uid);
				
				line = line.substring(line.indexOf(" ")+1);	
				double rating = Double.parseDouble(line);
				up.get(uid).addValue(rid,rating);
			}
			br.close();
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	/**
	 * read profile (liked / dislike) from file
	 * 
	 * @param p
	 * @param up
	 * @param ip
	 */
	public static void UsersItemsProfileLoader(String p, Map<Integer, ScoreCount<Integer>> up, Map<Integer, Set<Integer>> ip) {
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(p));
			
			String line;
			while ((line = br.readLine()) != null) {
				
				// skip the comment
				if(line.contains("#")) {
					continue;
				}
				
				Integer uid = Integer.parseInt(line.substring(0, line.indexOf("\t")));
				if(!up.containsKey(uid)) {
					up.put(uid, new ScoreCount<Integer>());
				}
				line = line.substring(line.indexOf("\t")+1);
				Integer rid = Integer.parseInt(line);
				up.get(uid).addValue(rid,1);
				
				if(!ip.containsKey(rid)) {
					Set<Integer> set = new HashSet<>();
					ip.put(rid, set);
				}
				ip.get(rid).add(uid);
			}
			br.close();
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	/**
	 * read 5 stars profile from file and define for each user if she liked / disliked items according to its average rating 
	 * 
	 * @param p
	 * @param up
	 * @param ip
	 */
	public static void UsersItemsStarValueProfileLoader(String p, Map<Integer, ScoreCount<Integer>> up, Map<Integer, Set<Integer>> ip) {
		
		// read file
		try {
			BufferedReader br = new BufferedReader(new FileReader(p));
			
			String line;
			while ((line = br.readLine()) != null) {
				Integer uid = Integer.parseInt(line.substring(0, line.indexOf("\t")));
				if(!up.containsKey(uid)) {
					up.put(uid, new ScoreCount<Integer>());
				}
				line = line.substring(line.indexOf("\t")+1);
				Integer rid = Integer.parseInt(line.substring(0, line.indexOf("\t")));
				
				if(!ip.containsKey(rid)) {
					Set<Integer> set = new HashSet<>();
					ip.put(rid, set);
				}
				
				ip.get(rid).add(uid);
				
				line = line.substring(line.indexOf("\t")+1);
				Integer rating = Integer.parseInt(line.substring(0, line.indexOf("\t")));
				
				up.get(uid).addValue(rid, rating);
			}
			br.close();
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	

	/**
	 * read profile (liked / dislike) from file
	 * 
	 * @param p
	 * @param up
	 * @param ip
	 */
	public static void UsersValueProfileLoader(String p, Map<Integer, ScoreCount<Integer>> up) {
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(p));
			
			String line;
			while ((line = br.readLine()) != null) {
				
				// skip the comment
				if(line.contains("#")) {
					continue;
				}
				
				Integer uid = Integer.parseInt(line.substring(0, line.indexOf(" ")));
				if(!up.containsKey(uid)) {
					up.put(uid, new ScoreCount<Integer>());
				}
				
				line = line.substring(line.indexOf(" ")+1);
				
				Integer rid = Integer.parseInt(line.substring(0, line.indexOf(" ")));
				
				line = line.substring(line.indexOf(" ")+1);	
				double rating = Double.parseDouble(line);
				up.get(uid).addValue(rid,rating);
			}
			br.close();
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	/**
	 * read profile (liked / dislike) from file
	 * 
	 * @param p
	 * @param up
	 * @param ip
	 */
	public static void UsersProfileLoader(String p, Map<Integer, ScoreCount<Integer>> up) {
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(p));
			
			String line;
			while ((line = br.readLine()) != null) {
				
				// skip the comment
				if(line.contains("#")) {
					continue;
				}
				
				Integer uid = Integer.parseInt(line.substring(0, line.indexOf("\t")));
				if(!up.containsKey(uid)) {
					up.put(uid, new ScoreCount<Integer>());
				}
				line = line.substring(line.indexOf("\t")+1);
				Integer rid = Integer.parseInt(line);
				up.get(uid).addValue(rid,1);				
			}
			br.close();
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	/**
	 * read 5 stars profile from file and define for each user if she liked / disliked items according to its average rating 
	 * 
	 * @param p
	 * @param up
	 * @param ip
	 */
	public static void UsersStarValueProfileLoader(String p, Map<Integer, ScoreCount<Integer>> up) {
		
		// read file
		try {
			BufferedReader br = new BufferedReader(new FileReader(p));
			
			String line;
			while ((line = br.readLine()) != null) {
				Integer uid = Integer.parseInt(line.substring(0, line.indexOf("\t")));
				if(!up.containsKey(uid)) {
					up.put(uid, new ScoreCount<Integer>());
				}
				line = line.substring(line.indexOf("\t")+1);
				Integer rid = Integer.parseInt(line.substring(0, line.indexOf("\t")));
				
				line = line.substring(line.indexOf("\t")+1);
				Integer rating = Integer.parseInt(line.substring(0, line.indexOf("\t")));
				
				up.get(uid).addValue(rid, rating);
			}
			br.close();
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
