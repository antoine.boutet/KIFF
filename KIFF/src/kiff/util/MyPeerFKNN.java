package kiff.util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import kiff.util.ScoreCount;



public class MyPeerFKNN extends MyPeer implements Serializable  {
	
	// arraylist sorted according to the number of similar liked item
	private ArrayList<Integer> itemBasedCandidates;
	
	public MyPeerFKNN(int id, Random r, ScoreCount<Integer> p, int sizeKnn) {
		
		super(id,r,p,sizeKnn);
		this.itemBasedCandidates = new ArrayList<Integer>();
	}
	

	 private void writeObject(ObjectOutputStream out) throws IOException {
			
//			out.writeObject(this.hops);
//			out.writeObject(this.targets);
//			out.writeObject(this.rumorScoreCount);
		}
	    
		private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
			
//			this.hops = (ArrayList<Hop>) in.readObject();
//			this.targets = (ArrayList<Integer>) in.readObject();
//			this.rumorScoreCount = (ScoreCount<Integer>) in.readObject();
		}

		
		public ArrayList<Integer> getItemBasedCandidates() {
			return itemBasedCandidates;
		}

		public void setItemBasedCandidates(ArrayList<Integer> itemBasedCandidates) {
			this.itemBasedCandidates = itemBasedCandidates;
		}

}

