package kiff.util;

import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import gnu.trove.map.hash.TIntDoubleHashMap;

public class Measures {
	

	public static void recallMeasure(ConcurrentHashMap<Integer, MyPeer> map, String path, int sizeknn, Map<Integer, TIntDoubleHashMap> best) {
	

		double recall = 0;
		double nb = 0;
		
		// for each peers
		for (Integer uid : map.keySet()) {
			
			nb=0;
			
			// get min value
			double minScore = Double.MAX_VALUE;
			for(double d : best.get(uid).values()) {
				if(minScore>d) {
					minScore = d;
				}
			}
			
			for(Double d : map.get(uid).getScores()) {
				if(d>0 && d>=minScore) {
					nb++;					
				} 
			}
			
			if(best.get(uid).size()<sizeknn) {
				nb += (sizeknn-best.get(uid).size());
			}
			
			nb /= sizeknn;		
			
			recall += nb;
			
		}
				
		recall /= map.size();
		
		System.out.println("Average recall " + recall);		
		
		Tools.log(path, recall + " " );
		
	}
	
	public static void candidateSetSizeMeasure(ConcurrentHashMap<Integer, MyPeer> map, String path) {
		

		double size = 0;
		
		// for each peers
		for (Integer uid : map.keySet()) {
					
			size += ((MyPeerFKNN)map.get(uid)).getItemBasedCandidates().size();	
			
		}
				
		size /= map.size();
		
		System.out.println("Average item-based candidate set size " + size);
		
		Tools.log(path, size + " ");
		
	}

	public static void nbSimilarityEvaluationMeasure(ConcurrentHashMap<Integer, MyPeer> map, String path) {
		
		double nbEval = 0;
		double scanrate = 0;
		long optimal =  map.size();
		optimal *= (map.size()-1);
		optimal /= 2;
		
		// for each peers
		for (Integer uid : map.keySet()) {
		
			nbEval += map.get(uid).getNbSimilarityEvaluation();			
		}
			
		scanrate += nbEval;
		scanrate /= optimal;
		
		nbEval /= map.size();
		
		System.out.println("Average similarity evaluation " + nbEval + " - " + scanrate);
		
		Tools.log(path, nbEval + " " + scanrate + " ");
		
	}

	public static void nbUpdateMeasure(ConcurrentHashMap<Integer, MyPeer> map, String path, double threshold) {
		
		double nb = 0;
		
		// for each peers
		for (Integer uid : map.keySet()) {
			
			nb += map.get(uid).getUpdate();	
			
//			if(p.getUpdate()>0)
//				System.out.println("node with update " + p.getPeerId());
		}
				
		nb /= map.size();
		
		System.out.println("Average update " + nb);
		
		Tools.log(path, " " + nb);
		
		if(nb < threshold) {
			System.out.println("TERMINAISON");		
		}
		
	}


}
