package kiff.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ScoreCount<C> implements Cloneable, Serializable {
	protected Map<C, Double> map;

	protected double norm;

	protected boolean normUpToDate;

	protected double sum;

	protected boolean sumUpToDate;

	public ScoreCount() {
		this.map = new HashMap<C, Double>();
		this.normUpToDate = false;
		this.sumUpToDate = false;
	}

	public ScoreCount(Map<C, Double> m) {
		this.map = m;
		this.normUpToDate = false;
		this.sumUpToDate = false;
	}

	public ScoreCount(ScoreCount<C> s) {
		synchronized (s) {
			this.map = new HashMap<C, Double>(s.map);
		}
		this.normUpToDate = false;
		this.sumUpToDate = false;
	}

	public void addValue(C c, double d) {
		this.normUpToDate = false;
		this.sumUpToDate = false;
		Double currentCount = this.map.get(c);
		if (currentCount == null) {
			this.map.put(c, d);
		} else {
			this.map.put(c, currentCount + d);
		}
	}

	public void add(ScoreCount<C> a) {
		this.normUpToDate = false;
		this.sumUpToDate = false;
		for (Entry<C, Double> en : a.map.entrySet()) {
			Double currentCount = this.map.get(en.getKey());
			if (currentCount == null) {
				this.map.put(en.getKey(), en.getValue());
			} else {
				this.map.put(en.getKey(), currentCount + en.getValue());
			}
		}
	}

	public final void setValue(C c, double d) {
		this.normUpToDate = false;
		this.sumUpToDate = false;
		this.map.put(c, d);
	}

	public final void removeValue(C c, double d) {
		this.normUpToDate = false;
		this.sumUpToDate = false;
		Double currentCount = this.map.get(c);
		if (currentCount != null) {
			double newCount = currentCount - d;
			if (newCount == 0) {
				this.map.remove(c);
			} else {
				this.map.put(c, newCount);
			}
		}
	}

	public final double getValue(C c) {
		Double val = this.map.get(c);
		if (val == null) {
			return 0;
		} else {
			return val;
		}
	}

	public final double getValueObject(Object c) {
		Double val = this.map.get(c);
		if (val == null) {
			return 0;
		} else {
			return val;
		}
	}

	public final List<C> getItems() {
		return new ArrayList<C>(this.map.keySet());
	}

	public List<C> getSortedItems() {
		List<C> res = new ArrayList<C>();
		if (!this.map.isEmpty()) {
			List<Entry<C, Double>> enl = new ArrayList<Entry<C, Double>>(
					this.map.size());
			for (Entry<C, Double> en : this.map.entrySet()) {
				enl.add(en);
			}
			Collections.sort(enl,
					Collections.reverseOrder((new EntryComparator())));
			for (Entry<C, Double> en : enl) {
				res.add(en.getKey());
			}
		}
		return res;
	}

	public final Map<C, Double> getMap() {
		return this.map;
	}

	public final double getNorm() {
		synchronized (map) {
			if (!this.normUpToDate) {
				this.norm = 0;
				for (Double d : this.map.values()) {
					this.norm += Math.pow(d, 2);
				}
				this.norm = Math.sqrt(this.norm);
				this.normUpToDate = true;
			}
		}
		return this.norm;
	}

	public final double getSum() {
		synchronized (map) {
			if (!this.sumUpToDate) {
				this.sum = 0;
				for (Double d : this.map.values()) {
					this.sum += d;
				}
				this.sumUpToDate = true;
			}
		}
		return this.sum;
	}

	public final double getScalar(ScoreCount<C> sc) {
		double scalar = 0;
		for (Entry<C, Double> m1e : this.map.entrySet()) {
			double m2score = sc.getValue(m1e.getKey());
			scalar += m2score * m1e.getValue();
		}
		return scalar;
	}

	public final double getCos(ScoreCount<C> sc) {
		double n1 = this.getNorm();
		if (n1 == 0) {
			return 0;
		}
		double n2 = sc.getNorm();
		if (n2 == 0) {
			return 0;
		}
		return this.getScalar(sc) / (n1 * n2);
	}
	
	public final double getJaccard(ScoreCount<C> sc) {
		
		int union = map.size();
		double inter = 0;
		
		for(C c : sc.getItems()) {
			if(map.containsKey(c)) {
				inter++;
			}
			else {
				union++;
			}
		}
		inter /= union;
		return inter;
	}

	public final int size() {
		return this.map.size();
	}

	public final void removeFloor(double val) {
		this.normUpToDate = false;
		this.sumUpToDate = false;
		List<C> remove = new ArrayList<C>();
		for (C c : this.map.keySet()) {
			if (this.map.get(c) < val) {
				remove.add(c);
			}
		}
		for (C c : remove) {
			this.map.remove(c);
		}
	}

	public final void trimToSize(int nb) {
		if (nb < this.size()) {
			this.normUpToDate = false;
			this.sumUpToDate = false;
			List<Entry<C, Double>> enl = new ArrayList<Entry<C, Double>>(
					this.map.entrySet());
			Collections.sort(enl, new EntryComparator());
			for (Entry<C, Double> en : enl.subList(0, this.size() - nb)) {
				this.map.remove(en.getKey());
			}
		}
	}

	public final void trimToSize(int nb, Collection<C> protect) {
		if (nb < this.size()) {
			this.normUpToDate = false;
			this.sumUpToDate = false;
			List<Entry<C, Double>> enl = new ArrayList<Entry<C, Double>>(
					this.map.size());
			for (Entry<C, Double> en : this.map.entrySet()) {
				if (!protect.contains(en.getKey())) {
					enl.add(en);
				}
			}
			Collections.sort(enl, new EntryComparator());
			for (Entry<C, Double> en : enl.subList(0, this.size() - nb)) {
				this.map.remove(en.getKey());
			}
		}
	}

	public final boolean contains(Object o) {
		return this.map.containsKey(o);
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		if (!this.map.isEmpty()
				&& this.map.keySet().iterator().next() instanceof Comparable) {
			List<C> lc = new ArrayList<C>(this.map.keySet());
			Collections.sort((List<Comparable>) lc);
			for (C c : lc) {
				sb.append(c + " " + this.map.get(c) + "\n");
			}
		} else {
			for (Entry<C, Double> e : this.map.entrySet()) {
				sb.append(e.getKey() + " " + e.getValue() + "\n");
			}
		}
		return sb.toString();
	}

	public final C getRandomBasedOnScore() {
		double s = this.getSum();
		double rand = Math.random() * s;
		for (Entry<C, Double> en : this.map.entrySet()) {
			if (en.getValue() > rand) {
				return en.getKey();
			} else {
				rand -= en.getValue();
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ScoreCount<C> clone() {
		ScoreCount<C> clone = null;
		try {
			clone = (ScoreCount<C>) super.clone();
		} catch (CloneNotSupportedException e) {
			System.err.println("error in ScoreCount clone");
			System.err.println(e);
			System.exit(-1);
		}
		clone.map = new HashMap<C, Double>(this.map);
		return clone;
	}

	public double getMax() {
		double max = Double.MIN_VALUE;
		for (Double val : this.map.values()) {
			if (val > max) {
				max = val;
			}
		}
		return max;
	}

	public double getMin() {
		double min = Double.MAX_VALUE;
		for (Double val : this.map.values()) {
			if (val < min) {
				min = val;
			}
		}
		return min;
	}

	public void remove(C c) {
		if (this.map.remove(c) != null) {
			this.normUpToDate = false;
			this.sumUpToDate = false;
		}
	}

	public void removeAll(Collection<C> c) {
		synchronized (map) {
			if (this.map.keySet().removeAll(c)) {
				this.normUpToDate = false;
				this.sumUpToDate = false;
			}
		}
	}

	public void retainAll(Collection<C> c) {
		synchronized (map) {
			if (this.map.keySet().retainAll(c)) {
				this.normUpToDate = false;
				this.sumUpToDate = false;
			}
		}
	}

	public void clear() {
		this.map.clear();
		this.normUpToDate = false;
		this.sumUpToDate = false;
	}

	public static void main(String[] args) {
		ScoreCount<String> s1 = new ScoreCount<String>();
		ScoreCount<String> s2 = new ScoreCount<String>();
		s1.setValue("haha", 12);
		s2.setValue("haha", -4.5);
		s1.setValue("hoho", 2);
		s2.setValue("huhu", 1.33);
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s1.getScalar(s2));
		System.out.println(s1.getCos(s2));
	}
}
