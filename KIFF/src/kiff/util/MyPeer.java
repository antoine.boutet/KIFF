package kiff.util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import kiff.util.ScoreCount;



public class MyPeer implements Serializable  {
	
	private Random random;
	private ScoreCount<Integer> myProfile;
	private int iteration;
	private int k;
	
	// neighborhood of the user
	private int[] topk;
	private double[] scores;
	private int currentlyIn;
	
	// fixed-size heap for neighborhood sorted according to the similarity 
//	private MyMinMaxPriorityQueue<HeapNode> heapFriends;
	
	private int update = 0;
	private int nbSimilarityEvaluation = 0;
	
	// candidate for the current cycle
	private ArrayList<Integer> candidates;
//	private ArrayList<Integer> interactPeers;

	
	public MyPeer(int id, Random r, ScoreCount<Integer> p, int sizeKnn) {
		
		this.random = r;
		this.myProfile = new ScoreCount<Integer>(p);
		this.candidates = new ArrayList<Integer>();
//		this.interactPeers = new ArrayList<Integer>();
//		this.heapFriends = new MyMinMaxPriorityQueue<HeapNode>(sizeKnn); 
		this.iteration = 0;		
		setTopk(new int[sizeKnn]);
		setScores(new double[sizeKnn]);
		currentlyIn = 0;
		this.k = sizeKnn;
	}
	

	 private void writeObject(ObjectOutputStream out) throws IOException {
			
//			out.writeObject(this.hops);
//			out.writeObject(this.targets);
//			out.writeObject(this.rumorScoreCount);
		}
	    
		private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
			
//			this.hops = (ArrayList<Hop>) in.readObject();
//			this.targets = (ArrayList<Integer>) in.readObject();
//			this.rumorScoreCount = (ScoreCount<Integer>) in.readObject();
		}

		
		public ScoreCount<Integer> getMyProfile() {			
			return myProfile;
		}
		
		public void setMyProfile(ScoreCount<Integer> p) {			
			myProfile = p;
		}

		public void incUpdate() {
			update++;
		}
		
		public void cleanUpdate() {	
			this.update=0;
		}

		public int getUpdate() {			
			return update;
		}
			
		public ArrayList<Integer> getCandidates() {
			return candidates;
		}

		public void setCandidates(ArrayList<Integer> candidates) {
			this.candidates = candidates;
		}

		public int getIteration() {
			return iteration;
		}

		public void incIteration() {
			this.iteration++;
		}
		
//		public MyMinMaxPriorityQueue<HeapNode> getHeapFriends() {
//			return heapFriends;
//		}
		
//		public String printHeapFriends() {
//			
//			String s = "";
//			
//			Iterator<HeapNode> it = heapFriends.getSortedHeap().iterator();
//			while(it.hasNext()) {
//				HeapNode elem = it.next();
//				s+=" "+elem.getId()+":"+elem.getScore()+":"+elem.getTimestamp();
//			}
//			
//			return s;
//		}

//		public void setHeapFriends(MyMinMaxPriorityQueue<HeapNode> heapFriends) {
//			this.heapFriends = heapFriends;
//		}

		public Random getRandom() {
			return random;
		}

//		public ArrayList<Integer> getInteractPeers() {
//			return interactPeers;
//		}
//
//
//		public void setInteractPeers(ArrayList<Integer> interactPeers) {
//			this.interactPeers = interactPeers;
//		}


		public int getNbSimilarityEvaluation() {
			return nbSimilarityEvaluation;
		}


		public void setNbSimilarityEvaluation(int nbSimilarityEvaluation) {
			this.nbSimilarityEvaluation = nbSimilarityEvaluation;
		}
		
		public void incNbSimilarityEvaluation() {
			this.nbSimilarityEvaluation++;
		}


		public int[] getTopk() {
			return topk;
		}


		public void setTopk(int[] topk) {
			this.topk = topk;
		}


		public double[] getScores() {
			return scores;
		}


		public void setScores(double[] scores) {
			this.scores = scores;
		}
		
		public int getSizeNeighborhood() {
			return currentlyIn;
		}
		
		public void setSizeNeighborhood(int value) {
			currentlyIn = value;
		}
		
		public int getSizeK() {
			return k;
		}
		
		public boolean updateNeighborhood(int user, double sc) {
			
			//if user already in the topk
			
			if(currentlyIn < k) {
				int insertPos;
				for (insertPos = currentlyIn - 1; insertPos >= 0 && sc > scores[insertPos]; insertPos--) {
				}
				insertPos++;
				for (int shiftPos = currentlyIn; shiftPos > insertPos; shiftPos--) {
					scores[shiftPos] = scores[shiftPos - 1];
					topk[shiftPos] = topk[shiftPos - 1];
				}
				scores[insertPos] = sc;
				topk[insertPos] = user;
				currentlyIn ++;
				return true;
			} 
			else if (sc > scores[k - 1]) {
				int insertPos;
				for (insertPos = k - 2; insertPos >= 0 && sc > scores[insertPos]; insertPos--) {
				}
				insertPos++;
				for (int shiftPos = k - 1; shiftPos > insertPos; shiftPos--) {
					scores[shiftPos] = scores[shiftPos - 1];
					topk[shiftPos] = topk[shiftPos - 1];
				}
				scores[insertPos] = sc;
				topk[insertPos] = user;
				return true;
			}
			
			return false;
		}
}

