package kiff.util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

import kiff.util.ScoreCount;



public class MyPeerNNDesc extends MyPeer implements Serializable  {
	
	private Set<Integer> _newOutcoming;
	private Set<Integer> _oldOutComing;
	private Set<Integer> _newIncomming;
	private Set<Integer> _oldIncomming;
	
	private boolean[] flags;
	
	public MyPeerNNDesc(int id, Random r, ScoreCount<Integer> p, int sizeKnn) {		
		super(id,r,p,sizeKnn);	
		
		flags = new boolean[sizeKnn];
		for(int i=0; i<flags.length; i++) {
			flags[i] = false;
		}
	}
	

	 private void writeObject(ObjectOutputStream out) throws IOException {
			
//		out.writeObject(this.hops);
//		out.writeObject(this.targets);
//		out.writeObject(this.rumorScoreCount);
	}
	    
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
			
//		this.hops = (ArrayList<Hop>) in.readObject();
//		this.targets = (ArrayList<Integer>) in.readObject();
//		this.rumorScoreCount = (ScoreCount<Integer>) in.readObject();
	}


	public Set<Integer> get_newOutcoming() {
		return _newOutcoming;
	}


	public void set_newOutcoming(Set<Integer> _newOutcoming) {
		this._newOutcoming = _newOutcoming;
	}


	public Set<Integer> get_oldOutComing() {
		return _oldOutComing;
	}


	public void set_oldOutComing(Set<Integer> _oldOutComing) {
		this._oldOutComing = _oldOutComing;
	}


	public Set<Integer> get_newIncomming() {
		return _newIncomming;
	}


	public void set_newIncomming(Set<Integer> _newIncomming) {
		this._newIncomming = _newIncomming;
	}


	public Set<Integer> get_oldIncomming() {
		return _oldIncomming;
	}


	public void set_oldIncomming(Set<Integer> _oldIncomming) {
		this._oldIncomming = _oldIncomming;
	}
	
	public boolean[] getFlags() {
		return flags;
	}
	
	// include the flag info
	public boolean updateNeighborhood(int user, double sc) {
		
		if(this.getSizeNeighborhood() < this.getSizeK()) {
			int insertPos;
			for (insertPos = this.getSizeNeighborhood() - 1; insertPos >= 0 && sc >= this.getScores()[insertPos]; insertPos--) {
				if(this.getTopk()[insertPos] == user) {
					return false;
				}
			}
			insertPos++;
			for (int shiftPos = this.getSizeNeighborhood(); shiftPos > insertPos; shiftPos--) {
				this.getScores()[shiftPos] = this.getScores()[shiftPos - 1];
				this.getTopk()[shiftPos] = this.getTopk()[shiftPos - 1];
				this.getFlags()[shiftPos] = this.getFlags()[shiftPos - 1];
			}
			this.getScores()[insertPos] = sc;
			this.getTopk()[insertPos] = user;
			this.getFlags()[insertPos] = true;
			this.setSizeNeighborhood(this.getSizeNeighborhood()+1);
			
			return true;
		} 
		else if (sc > this.getScores()[this.getSizeK() - 1]) {
			int insertPos;
			for (insertPos = this.getSizeK() - 2; insertPos >= 0 && sc >= this.getScores()[insertPos]; insertPos--) {
				if(this.getTopk()[insertPos] == user) {
					return false;
				}
			}
			insertPos++;
			for (int shiftPos = this.getSizeK() - 1; shiftPos > insertPos; shiftPos--) {
				this.getScores()[shiftPos] = this.getScores()[shiftPos - 1];
				this.getTopk()[shiftPos] = this.getTopk()[shiftPos - 1];
				this.getFlags()[shiftPos] = this.getFlags()[shiftPos - 1];
			}
			this.getScores()[insertPos] = sc;
			this.getTopk()[insertPos] = user;
			this.getFlags()[insertPos] = true;
			
			return true;
		}
		
		return false;
	}

}

