# KIFF: An impressively fast and efficient JAVA library for KNN construction


This package implements the KIFF algorithm reported in [1]. KIFF is a generic, fast and scalable K-Nearest-Neighbor graph construction algorithm. This algorithm connects each object to its k most similar counterparts, according to a given similarity metric.
In term of comparison, this package implements also HYREC [2] and NN-DESCENT [3]. 

The standalone program implements cosine similarity only, however this library supports arbitrary similarity measures.

THIS SOFTWARE DOES NOT HAVE ANY GUARANTEE ON THE ACCURACY OF THE RESULT.

## Using the Program

mkdir bestKNN results

The following exemple show how to do for wikipedia dataset, the same procedure has to be perform for the other datasets.

### KIFF

USAGE: {dataset 3:arxiv 4:wikipedia 5:gowalla 6:dbpedia} k cycleTermination thresholdTermination best(boolean) log(boolean) resultsPath bestPath

use kiff to create the true knn:
$ java -jar kiff.jar 4 20 100 0 true false results/ bestKNN/bestKNN-wikipedia-k20

then run kiff and activate log
$ java -jar kiff.jar 4 20 100 0.001 false true results/kiff- bestKNN/

run kiff without log
$ java -jar kiff.jar 4 20 100 0.001 false false results/kiff- bestKNN/


### NN-DESCENT

USAGE: {dataset 3:arxiv 4:wikipedia 5:gowalla 6:dbpedia} k cycleTermination thresholdTermination log(boolean) resultsPath bestPath

run nndescent and activate log
$ java -jar nndescent.jar 4 20 100 0.001 true results/nndescent- bestKNN/

run nndescent without log
$ java -jar nndescent.jar 4 20 100 0.001 false results/nndescent- bestKNN/


### HYREC

USAGE: {dataset 3:arxiv 4:wikipedia 5:gowalla 6:dbpedia} k random cycleTermination thresholdTermination log(boolean) resultsPath bestPath

run hyrec and activate log
$ java -jar hyrec.jar 4 20 100 0.001 true results/hyrec- bestKNN/

run hyrec without log
$ java -jar hyrec.jar 4 20 100 0.001 false results/hyrec- bestKNN/


## Using the Library

### KIFF

	public static void main(String[] args) {
				  
		if(args.length<8) {
			System.out.println("USAGE: {dataset 3:arxiv 4:wikipedia 5:gowalla 6:dbpedia} "
					+ "k cycleTermination thresholdTermination best(boolean) log(boolean) resultsPath bestPath");
			System.exit(0);
		}
		
		int dataset = Integer.parseInt(args[0]);			// dataset
		int k = Integer.parseInt(args[1]);					// size knn
		int c = Integer.parseInt(args[2]);					// cycleTerminaison
		double t = Double.parseDouble(args[3]);				//  terminationThreshold
		Boolean b = Boolean.parseBoolean(args[4]); 			// 	bestMatch
		Boolean l = Boolean.parseBoolean(args[5]); 			// log
		String r = args[6]; 								// resultsPath
		String bestKNNPath = args[7];						// string best knn (for recall measurement)
		
		
		KIFF kiff = new KIFF(k, dataset, t, b, r, c, l, bestKNNPath);
		
		
		
		if(b) {			
			// get the knn graph with the distance between users
			Hashtable<Integer, ScoreCount<Integer>> knn = kiff.getKNNValueGraph();
					
			Tools.saveGraphValue(bestKNNPath, knn);
			
			Tools.saveObject(bestKNNPath, knn);
		}
		
	}

### NN-DESCENT


		if(args.length<7) {
			System.out.println("USAGE: {dataset 3:arxiv 4:wikipedia 5:gowalla 6:dbpedia} "
					+ "k cycleTermination thresholdTermination log(boolean) resultsPath bestPath");
			System.exit(0);
		}
		
		int dataset = Integer.parseInt(args[0]);			// dataset
		int k = Integer.parseInt(args[1]);					// size knn
		int c = Integer.parseInt(args[2]);					// cycleTerminaison
		double t = Double.parseDouble(args[3]);				//  terminationThreshold//		
		Boolean l = Boolean.parseBoolean(args[4]); 			// log
		String r = args[5]; 								// resultsPath
		String bestKNNPath = args[6];						// string best knn (for recall measurement)
		
		NNDESCENT kiff = new NNDESCENT(k, dataset, t, r, c, l, bestKNNPath);
		
	}

### HYREC


	public static void main(String[] args) {
		
		if(args.length<8) {
			System.out.println("USAGE: {dataset 3:arxiv 4:wikipedia 5:gowalla 6:dbpedia} "
					+ "k random cycleTermination thresholdTermination log(boolean) resultsPath bestPath");
			System.exit(0);
		}
		
		int dataset = Integer.parseInt(args[0]);			// dataset
		int k = Integer.parseInt(args[1]);					// size knn
		int sr = Integer.parseInt(args[2]);					// size random
		int c = Integer.parseInt(args[3]);					// cycleTerminaison
		double t = Double.parseDouble(args[4]);				//  terminationThreshold//		
		Boolean l = Boolean.parseBoolean(args[5]); 			// log
		String r = args[6]; 								// resultsPath
		String bestKNNPath = args[7];						// string best knn (for recall measurement)
		
		
		HYREC kiff = new HYREC(k, dataset, t, r, c, l, bestKNNPath, sr);
		
	}

## Datasets

This repository contains three publicly available datasets [4] including bibliographic collections (Arxiv), voting systems (Wikipedia) and on-line social networks with geo-location information (Gowalla), as well as a snapshop of the DBLP databased (bibliographic collections) collected in September 2015. The movielens dataset is also provided through different versions with varying value of density. More details on datasets are provided in [1].

## Contact

Please contact the author (antoine.boutet@gmail.com) for problems and bug report.

## Reference

[1] Antoine Boutet, Anne-Marie Kermarrec, Nupur Mittal, Francois Taiani. Being prepared in a sparse world: the case of KNN graph construction. ICDE 2016, Finland.

[2] Antoine Boutet, Davide Frey, Rachid Guerraoui, Anne-Marie Kermarrec, Rhicheek Patra. HyRec: Leveraging Browsers for Scalable Recommenders. Middleware 2014, Dec 2014, Bordeaux, France.

[3] Wei Dong, Charikar Moses, and Kai Li. Efficient k-nearest neighbor graph construction for generic similarity measures. WWW 2011.

[4] SNAP Datasets: http://snap.stanford.edu/data
